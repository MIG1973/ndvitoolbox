package ndviToolBox.model;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.ObjectMapper;

import com.phytech.model.GenericAlert;
import com.phytech.model.Enumerations.AlertType;


public class LowGrowthAlert extends GenericAlert {

	public LowGrowthAlert() {
		type = AlertType.NDVI_LOW_GROWTH_ALERT;
		PixelsAlertData alertData = new PixelsAlertData();
		setData(alertData);
	}

	public void addGroup(GroupOfPixel group) {
		((PixelsAlertData) this.data).groups.add(group);
	}

	@JsonIgnore
	public List<GroupOfPixel> getGroups() {
		return ((PixelsAlertData) this.data).groups;
	}

	@JsonIgnore
	public int getPlot_id() {
		return ((PixelsAlertData) this.data).plotId;
	}

	public void setPlotId(int plotId) {
		((PixelsAlertData) this.data).plotId = plotId;
	}

	@JsonIgnore
	public long getDate() {
		return ((PixelsAlertData) this.data).date;
	}

	public void setDate(long date) {
		((PixelsAlertData) this.data).date = date;
	}

	@JsonIgnore
	public double getResolution() {
		return ((PixelsAlertData) this.data).resolution;
	}

	public void setResolution(double resolution) {
		((PixelsAlertData) this.data).resolution = resolution;
	}
	
	public String toJson() {
		String json = "{}";

		ObjectMapper objectMapper = new ObjectMapper();
		try {
			json = objectMapper.writeValueAsString(this);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

}
