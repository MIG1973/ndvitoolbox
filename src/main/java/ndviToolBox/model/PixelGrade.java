package ndviToolBox.model;

public class PixelGrade {
	private int I;
	private int J;
	private double grade;

	public PixelGrade(int i, int j, int score){
		setI(i);
		setJ(j);
		setGrade(score);
	}
	
	public int getI() {
		return I;
	}

	public void setI(int i) {
		I = i;
	}

	public double getGrade() {
		return grade;
	}

	public void setGrade(double d) {
		this.grade = d;
	}

	public int getJ() {
		return J;
	}

	public void setJ(int j) {
		J = j;
	}

}
