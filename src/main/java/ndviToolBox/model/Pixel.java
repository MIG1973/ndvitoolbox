package ndviToolBox.model;

import java.util.List;
import java.util.Objects;

import org.apache.commons.math3.ml.clustering.Clusterable;

import geodesy.GeoConvertor;
import geodesy.Geo_Point;
import geodesy.UTM_Point;

public class Pixel implements Clusterable {
	private UTM_Point coordinate;
	private Double value;
	private int id;
	private int maxLines=0;
	private int tessellate = 1;
	private int polyStyle=0;
	private List<Geo_Point> original;
	

	public UTM_Point getCoordinate() {
		return coordinate;
	}

	public Geo_Point getGeoCoordinate() {
		return GeoConvertor.getInstance().UTM2Deg(coordinate);
	}

	public void setCoordinate(UTM_Point coordinate) {
		this.coordinate = coordinate;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public double[] getPoint() {

		double[] res = new double[1];
		if (value != null) {
			res[0] = value.doubleValue();
		} else {
			res[0] = -1.0;
		}
		return res;

	}

	@Override
	public String toString() {
		return coordinate.toJson() + " value: " + value;
	}

	public int getMaxLines() {
		return maxLines;
	}

	public void setMaxLines(int maxLines) {
		this.maxLines = maxLines;
	}

	public int getTessellate() {
		return tessellate;
	}

	public void setTessellate(int tessellate) {
		this.tessellate = tessellate;
	}

	public int getPolyStyle() {
		return polyStyle;
	}

	public void setPolyStyle(int polyStyle) {
		this.polyStyle = polyStyle;
	}

	public List<Geo_Point> getOriginal() {
		return original;
	}

	public void setOriginal(List<Geo_Point> original) {
		this.original = original;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(coordinate.getEasting(), coordinate.getNorthing(),value);
	}
}
