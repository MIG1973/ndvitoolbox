package ndviToolBox.model;

import java.util.ArrayList;
import java.util.List;
import org.codehaus.jackson.map.ObjectMapper;

public class PixelsAlertData {
	protected int plotId;
	protected long date;
	protected double resolution;
	protected List<GroupOfPixel> groups;

	public PixelsAlertData() {
		groups = new ArrayList<GroupOfPixel>();
	}

	public void addGroup(GroupOfPixel group) {
		groups.add(group);
	}

	public List<GroupOfPixel> getGroups() {
		return groups;
	}

	public int getPlot_id() {
		return plotId;
	}

	public void setPlotId(int plotId) {
		this.plotId = plotId;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public double getResolution() {
		return resolution;
	}

	public void setResolution(double resolution) {
		this.resolution = resolution;
	}

	public String toJson() {
		String json = "{}";

		ObjectMapper objectMapper = new ObjectMapper();
		try {
			json = objectMapper.writeValueAsString(this);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

}
