package ndviToolBox.runner;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.log4j.Logger;
import com.phytech.model.ProjectMetadata;
import com.phytech.utils.Constants;
import com.phytech.utils.PhytechApi;
import com.phytech.utils.PropertiesManager;

import geodesy.GeoConvertor;
import geodesy.Geo_Point;
import geodesy.UTM_Point;
import ndviToolBox.model.LowGrowthAlert;
import ndviToolBox.model.Channel;
import ndviToolBox.model.GroupOfPixel;
import ndviToolBox.model.Image;
import ndviToolBox.model.OneChannelImage;
import ndviToolBox.model.Pixel;
import ndviToolBox.model.Point;
import ndviToolBox.model.RGB_Image;

public class LowGrowthDetector {
	private static final Logger log = Logger.getLogger(LowGrowthDetector.class);
	private static final int pixelResolution = 10;
	private static Pixel[][] ndvi_diff_matrix = null;
	private static boolean DEBUG = false;
	private static boolean inited = false;
	private static double entirePlotAlertRatio = Constants.DEFAULT_ENTIRE_PLOT_LOW_GROWTH_ALERT;
	private static double singlePixelAlertRatio = Constants.DEFAULT_SINGLE_PIXEL_LOW_GROWTH_ALERT;
	private static double stdev_for_low_growth = Constants.DEFAULT_STDEV_FOR_LOW_GROWTH_ALERT;
	private static int min_size_of_low_growth_area = Constants.DEFAULT_MIN_SIZE_OF_LOW_GROWTH_AREA;

	public static void main(String[] args) {
		PropertiesManager.initConfiguration(System.getenv(Constants.CONFIGURATION_FILE_VARIABLE_NAME));

		String oldKml = "C:\\Users\\Misha\\Documents\\Debug\\40369-2018-11-17.kml";
		String rgbOld = "C:\\Users\\Misha\\Documents\\Debug\\40369-2018-11-17_rgb.kml";

		String newKml = "C:\\Users\\Misha\\Documents\\Debug\\40369-2019-01-01.kml";
		String rgbNew = "C:\\Users\\Misha\\Documents\\Debug\\40369-2019-01-01_rgb.kml";

		ProjectMetadata pmd = PhytechApi.getProjectMetaData(40369);

		LowGrowthAlert alert = locateLowGrowthPixels(oldKml, rgbOld, newKml, rgbNew, pmd);

		System.out.println(alert.toJson());

		System.out.println("Done");
	}

	private static void initParams() {

		if (System.getenv(Constants.ENTIRE_PLOT_LOW_GROWTH_ALERT_ENV_VARIABLE_NAME) != null) {
			entirePlotAlertRatio = Double
					.parseDouble(System.getenv(Constants.ENTIRE_PLOT_LOW_GROWTH_ALERT_ENV_VARIABLE_NAME));
		}

		if (System.getenv(Constants.SINGLE_PIXEL_LOW_GROWTH_ALERT_ENV_VARIABLE_NAME) != null) {
			singlePixelAlertRatio = Double
					.parseDouble(System.getenv(Constants.SINGLE_PIXEL_LOW_GROWTH_ALERT_ENV_VARIABLE_NAME));
		}

		if (System.getenv(Constants.STDEV_FOR_LOW_GROWTH_ALERT_ENV_VARIABLE_NAME) != null) {
			stdev_for_low_growth = Double
					.parseDouble(System.getenv(Constants.STDEV_FOR_LOW_GROWTH_ALERT_ENV_VARIABLE_NAME));
		}

		if (System.getenv(Constants.MINIMAL_SIZE_OF_LOW_GROWTH_AREA_ENV_VARIABLE_NAME) != null) {
			min_size_of_low_growth_area = Integer
					.parseInt(System.getenv(Constants.MINIMAL_SIZE_OF_LOW_GROWTH_AREA_ENV_VARIABLE_NAME));
		}

		log.info("Minimal size (px) of low growth area " + min_size_of_low_growth_area);

		log.info("Entire plot NDVI average ratio " + entirePlotAlertRatio);
		log.info("Single pixel NDVI ratio " + singlePixelAlertRatio);
		log.info("STDEV from average " + stdev_for_low_growth);
	}

	public static LowGrowthAlert locateLowGrowthPixels(String oldNDVI, String oldRGB, String newNDVI, String newRGB,
			int projectId) {
		ProjectMetadata pmd = PhytechApi.getProjectMetaData(projectId);
		return locateLowGrowthPixels(oldNDVI, oldRGB, newNDVI, newRGB, pmd);
	}

	public static LowGrowthAlert locateLowGrowthPixels(String oldNDVI, String oldRGB, String newNDVI, String newRGB,
			ProjectMetadata pmd) {

		if (System.getenv("DEBUG") != null) {
			DEBUG = Boolean.parseBoolean(System.getenv("DEBUG"));
		}

		LowGrowthAlert alert = new LowGrowthAlert();

		if (pmd == null) {
			return alert;
		}

		alert.setPlotId(pmd.getPlotId());
		alert.setDate(System.currentTimeMillis());
		alert.setResolution(pixelResolution);

		if (!inited) {
			initParams();
			inited = true;
		}

		log.debug("Old NDVI " + oldNDVI + " Old RGB " + oldRGB + " New NDVI " + newNDVI + " New RGB " + newRGB
				+ " Crop " + pmd.getCropType());

		File ndviFileOld = new File(oldNDVI);
		File ndviFileNew = new File(newNDVI);

		Image imageOld = new Image();
		Image imageNew = new Image();

		// parse 2 NDVI files
		try {
			if (oldNDVI != null && ndviFileOld.exists()) {
				imageOld.parse(ndviFileOld);
			} else {
				log.error("Old file doesn't exists");
				return alert;

			}

			if (newNDVI != null && ndviFileNew.exists()) {
				imageNew.parse(ndviFileNew);
			} else {
				log.error("New file doesn't exists");
				return alert;

			}
		} catch (Exception e) {
			log.error("Error during kml parser");
			return alert;

		}

		if (imageOld.getPixels().isEmpty() || imageOld.getPixels().isEmpty()) {
			log.error("Empty file");
			return alert;
		}

		// Fix possible difference in MBR between old and new NDVI
		UTM_Point commonMin = new UTM_Point(Math.max(imageOld.getMin().getEasting(), imageNew.getMin().getEasting()),
				Math.max(imageOld.getMin().getNorthing(), imageNew.getMin().getNorthing()), imageNew.getMin().getZone(),
				imageNew.getMin().getHemisphere());

		UTM_Point commonMax = new UTM_Point(Math.min(imageOld.getMax().getEasting(), imageNew.getMax().getEasting()),
				Math.min(imageOld.getMax().getNorthing(), imageNew.getMax().getNorthing()), imageNew.getMax().getZone(),
				imageNew.getMax().getHemisphere());

		imageOld.limitBoundary(commonMin, commonMax);
		imageNew.limitBoundary(commonMin, commonMax);

		if (DEBUG) {
			log.debug("Boundaries: " + " LB: " + commonMin.toJson() + " RT: " + commonMax.toJson());
			log.debug("old image dimensions " + imageOld.numX() + "x" + imageOld.numY());
			log.debug("new image dimensions " + imageNew.numX() + "x" + imageNew.numY());
		}

		// clean external borders
		SobelEdgeDetector extSobel = new SobelEdgeDetector();
		extSobel.process(imageOld);

		if (DEBUG) {
			extSobel.ImageWrite("C:\\Temp\\DirOld.png", extSobel.Direction);
			extSobel.ImageWrite("C:\\Temp\\MagOld.png", extSobel.Magnitute);
		}

		// remove pixels near the edges (external borders of NDVI)
		imageOld.cleanOutBorders(extSobel);

		extSobel.process(imageNew);
		if (DEBUG) {
			extSobel.ImageWrite("C:\\Temp\\DirNew.png", extSobel.Direction);
			extSobel.ImageWrite("C:\\Temp\\MagNew.png", extSobel.Magnitute);
		}

		imageNew.cleanOutBorders(extSobel);

		// calculate statistics for both NDVI files
		imageNew.calcStatistics();
		imageOld.calcStatistics();

		if (entirePlotAlert(imageOld, imageNew)) {
			GroupOfPixel entirePlot = new GroupOfPixel();
			entirePlot.setGroupId(1);
			entirePlot.setSize(imageNew.pixels.size());

			for (Pixel px : imageNew.pixels) {
				Geo_Point gp = GeoConvertor.getInstance().UTM2Deg(px.getCoordinate());
				entirePlot.addPoint(gp);
			}

			alert.addGroup(entirePlot);
			log.info("Entire plot Alert");
		} else {
			// parse 2 RGB files
			File rgbFileOld = null;
			OneChannelImage rImageOld = null;

			if (oldRGB != null) {
				rgbFileOld = new File(oldRGB);
				if (!rgbFileOld.exists()) {
					rgbFileOld = null;
				} else {
					rImageOld = new OneChannelImage(Channel.R);
					try {
						rImageOld.parse(rgbFileOld);
						rImageOld.cleanOutBorders(extSobel);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
			File rgbFileNew = null;
			OneChannelImage rImageNew = null;
			if (newRGB != null) {
				rgbFileNew = new File(newRGB);
				if (!rgbFileNew.exists()) {
					rgbFileNew = null;
				} else {
					try {
						rImageNew = new OneChannelImage(Channel.R);
						rImageNew.parse(rgbFileNew);
						rImageNew.cleanOutBorders(extSobel);
						if (DEBUG) {
							rImageNew.printMatrix("C:\\Temp\\");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}

			RGB_Image rgbOld = FootPathDetector.findFootPath(rgbFileOld);
			RGB_Image rgbNew = FootPathDetector.findFootPath(rgbFileNew);

			int cloudPixelsNum = 0;
			if (rImageOld != null) {
				cloudPixelsNum += rImageOld.getCloudPixelsNumber(rgbOld);
			}

			if (rImageNew != null) {
				cloudPixelsNum += rImageNew.getCloudPixelsNumber(rgbNew);
			}

			if (cloudPixelsNum > 0) {
				log.info(cloudPixelsNum + " pixels detected as clouds. Skip");
				return alert;
			}

			// difference (new-old) NDVI values
			ndvi_diff_matrix = new Pixel[imageNew.numX()][imageNew.numY()];
			int[][] signifficant = new int[imageNew.numX()][imageNew.numY()];
			DescriptiveStatistics stat = new DescriptiveStatistics();

			for (int r = 0; r < imageNew.numX(); r++) {
				for (int c = 0; c < imageNew.numY(); c++) {
					if (imageOld.getMatrix()[r][c] != null && imageNew.getMatrix()[r][c] != null) {
						ndvi_diff_matrix[r][c] = new Pixel();
						ndvi_diff_matrix[r][c].setCoordinate(imageNew.getMatrix()[r][c].getCoordinate());
						double val = imageNew.getMatrix()[r][c].getValue() - imageOld.getMatrix()[r][c].getValue();
						ndvi_diff_matrix[r][c].setValue(val);
						stat.addValue(val);

					}
				}
			}

			List<Geo_Point> onPathPoints = new ArrayList<Geo_Point>();
			for (int r = 0; r < imageNew.numX(); r++) {
				for (int c = 0; c < imageNew.numY(); c++) {

					if ((ndvi_diff_matrix[r][c] != null) && (ndvi_diff_matrix[r][c].getValue() < singlePixelAlertRatio
							|| (ndvi_diff_matrix[r][c].getValue() < 0
									&& ndvi_diff_matrix[r][c].getValue() < stat.getMean()
											- stdev_for_low_growth * stat.getStandardDeviation()))) {

						boolean onPath = false;

						onPath = (rgbOld != null && rgbOld.isPixelOnPath(r, c))
								|| (rgbNew != null && rgbNew.isPixelOnPath(r, c));

						if (onPath && imageNew.getMatrix()[r][c] != null) {
							onPathPoints.add(
									GeoConvertor.getInstance().UTM2Deg(imageNew.getMatrix()[r][c].getCoordinate()));
						} else {
							signifficant[r][c] = 1;
						}

					}

				}
			}
			log.info("Following significant points were marked as on/near foot-path");
			if (!onPathPoints.isEmpty()) {
				for (Geo_Point gp : onPathPoints) {
					log.info(gp.toString());

				}
			}

			Map<Integer, List<Point>> labeledMap = ImageSegmentator.labelImageToMap(imageNew.numX() * imageNew.numY(),
					signifficant);

			if (DEBUG) {
				saveToImage(imageNew.numY(), imageNew.numX(), labeledMap, ".//debug_image.bmp");
			}

			for (Integer label : labeledMap.keySet()) {
				if (labeledMap.get(label).size() > min_size_of_low_growth_area) {
					GroupOfPixel group = new GroupOfPixel();
					group.setGroupId(label);
					group.setSize(labeledMap.get(label).size());
					log.debug("Label " + label + " size " + labeledMap.get(label).size());
					for (Point p : labeledMap.get(label)) {
						UTM_Point utm = ndvi_diff_matrix[p.getI()][p.getJ()].getCoordinate();
						Geo_Point geo = GeoConvertor.getInstance().UTM2Deg(utm);
						if (DEBUG) {
							// log.debug("[X,Y] [" + p.getI() + "," + p.getJ() + "] UTM [E,N,Zone] [" +
							// utm.getEasting()
							// + "," + utm.getNorthing() + "," + utm.getZone() + "]");
							log.debug(
									"[X,Y] [" + p.getI() + "," + p.getJ() + "] Geo [Lat,Lon] [" + geo.toString() + "]");

						}
						group.addPoint(geo);

					}
					if (DEBUG) {
						log.debug("-----------------------");
					}

					alert.addGroup(group);
				}
			}
		}

		return alert;
	}

	private static boolean entirePlotAlert(Image imageOld, Image imageNew) {

		double oldAverage = imageOld.getStatistics().descriptiveStatistics.getMean();
		double newAverage = imageNew.getStatistics().descriptiveStatistics.getMean();

		log.debug("old average " + oldAverage + " new average " + newAverage);
		// return (newAverage < oldAverage && (newAverage - oldAverage) / oldAverage <
		// entirePlotAlertRatio);
		return false;

	}

	public static void saveToImage(int width, int height, Map<Integer, List<Point>> data, String path) {

		BufferedImage res = new BufferedImage(height, width, BufferedImage.TYPE_INT_RGB);
		int ll = 0;

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				res.setRGB(i, j, Color.LIGHT_GRAY.getRGB());

			}
		}

		for (Integer label : data.keySet()) {
			if (data.get(label).size() > 5) {
				for (Point p : data.get(label)) {
					res.setRGB(height - p.getI() - 1, p.getJ(), labelToColor(ll).getRGB());
				}

				ll++;
			}
		}

		RenderedImage rendImage = res;
		try {
			ImageIO.write(rendImage, "bmp", new File(path));
		} catch (IOException e) {
			log.error("Error when rendering image", e);
		}

	}

	public static void saveToImage(int[][] data, String path) {

		BufferedImage res = new BufferedImage(data.length, data[0].length, BufferedImage.TYPE_INT_RGB);

		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[0].length; j++) {
				if (data[i][j] > 0) {
					res.setRGB(i, j, Color.BLACK.getRGB());
				} else {
					res.setRGB(i, j, Color.WHITE.getRGB());
				}

			}
		}

		RenderedImage rendImage = res;
		try {
			ImageIO.write(rendImage, "bmp", new File(path));
		} catch (IOException e) {
			log.error("Error when rendering image", e);
		}

	}

	public static Color labelToColor(int label) {
		switch (label) {
		case 0:
			return Color.WHITE;
		case 1:
			return Color.RED;
		case 2:
			return Color.GREEN;
		case 3:
			return Color.YELLOW;
		case 4:
			return Color.BLUE;
		case 5:
			return Color.ORANGE;
		case 6:
			return Color.MAGENTA;
		case 7:
			return Color.PINK;
		case 8:
			return Color.GRAY;
		case 9:
			return Color.CYAN;
		case 10:
			return Color.BLACK;

		default:
			return new Color(label + 25, label + 25, label + 25);
		}
	}

}
