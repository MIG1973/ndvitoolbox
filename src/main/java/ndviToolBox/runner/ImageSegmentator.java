package ndviToolBox.runner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import ndviToolBox.model.Point;
import ndviToolBox.model.SizedStack;

public class ImageSegmentator {
	public static void main(String[] args) {
		int[][] img = new int[9][9];

		img[0][5] = 1;
		img[1][2] = 1;
		img[1][3] = 1;
		img[2][3] = 1;
		img[2][5] = 1;
		img[3][1] = 1;

		img[3][4] = 1;
		img[3][7] = 1;
		img[4][5] = 1;
		// img[5][7]=1;
		img[6][4] = 1;

		img[6][5] = 1;
		img[6][6] = 1;
		img[8][7] = 1;

		System.out.println("Input");
		for (int i = 0; i < img.length; i++) {
			String row = "";
			for (int j = 0; j < img[i].length; j++) {
				row += img[i][j] + " ";
			}

			System.out.println(row);
		}

		int[][] labels = labelImage(81, img);
		System.out.println("Output");

		for (int i = 0; i < labels.length; i++) {
			String row = "";
			for (int j = 0; j < labels[i].length; j++) {
				row += labels[i][j] + " ";
			}

			System.out.println(row);
		}

	}

	public static int[][] labelImage(int stackSize, int[][] image) {
		return labelImage(stackSize, image, false);
	}

	public static int[][] labelImage(int stackSize, int[][] image, boolean withDiagonals) {
		int nRow = image.length;
		int nColumn = image[0].length;
		int labelIndex = 1;
		int[] pos;
		Stack<int[]> stack = new SizedStack<int[]>(stackSize);
		// prepare 2d array for labels
		int[][] labeledImage = new int[nRow][nColumn];
		for (int r = 1; r < nRow - 1; r++)
			for (int c = 1; c < nColumn - 1; c++) {
				if (image[r][c] == 0)
					continue;
				if (labeledImage[r][c] > 0)
					continue;
				/* encountered unlabeled foreground pixel at position r, c */
				/* push the position on the stack and assign label */
				stack.push(new int[] { r, c });
				labeledImage[r][c] = labelIndex;
				/* start the float fill */
				while (!stack.isEmpty()) {
					pos = stack.pop();
					int i = pos[0];
					int j = pos[1];
					if (withDiagonals) {
						if (image[i - 1][j - 1] == 1 && labeledImage[i - 1][j - 1] == 0) {
							stack.push(new int[] { i - 1, j - 1 });
							labeledImage[i - 1][j - 1] = labelIndex;
						}
					}
					if (image[i - 1][j] == 1 && labeledImage[i - 1][j] == 0) {
						stack.push(new int[] { i - 1, j });
						labeledImage[i - 1][j] = labelIndex;
					}
					if (withDiagonals) {
						if (image[i - 1][j + 1] == 1 && labeledImage[i - 1][j + 1] == 0) {
							stack.push(new int[] { i - 1, j + 1 });
							labeledImage[i - 1][j + 1] = labelIndex;
						}
					}
					if (image[i][j - 1] == 1 && labeledImage[i][j - 1] == 0) {
						stack.push(new int[] { i, j - 1 });
						labeledImage[i][j - 1] = labelIndex;
					}
					if (image[i][j + 1] == 1 && labeledImage[i][j + 1] == 0) {
						stack.push(new int[] { i, j + 1 });
						labeledImage[i][j + 1] = labelIndex;
					}
					if (withDiagonals) {
						if (image[i + 1][j - 1] == 1 && labeledImage[i + 1][j - 1] == 0) {
							stack.push(new int[] { i + 1, j - 1 });
							labeledImage[i + 1][j - 1] = labelIndex;
						}
					}
					if (image[i + 1][j] == 1 && labeledImage[i + 1][j] == 0) {
						stack.push(new int[] { i + 1, j });
						labeledImage[i + 1][j] = labelIndex;
					}
					if (withDiagonals) {
						if (image[i + 1][j + 1] == 1 && labeledImage[i + 1][j + 1] == 0) {
							stack.push(new int[] { i + 1, j + 1 });
							labeledImage[i + 1][j + 1] = labelIndex;
						}
					}
				}
				labelIndex++;
			}
		return labeledImage;
	}

	public static Map<Integer, List<Point>> labelImageToMap(int stackSize, int[][] image) {

		int[][] labeled = labelImage(stackSize, image,true);

		Map<Integer, List<Point>> res = new HashMap<Integer, List<Point>>();

		for (int r = 0; r < image.length; r++) {
			for (int c = 0; c < image[r].length; c++) {
				int label = labeled[r][c];
				if (label > 0) {
					List<Point> coordinates = res.remove(label);
					if (coordinates == null) {
						coordinates = new ArrayList<Point>();
					}
					coordinates.add(new Point(r, c));
					res.put(label, coordinates);
				}
			}
		}

		return res;
	}

}
