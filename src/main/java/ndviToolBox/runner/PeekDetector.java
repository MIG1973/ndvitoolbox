package ndviToolBox.runner;

import java.io.File;
import org.apache.log4j.Logger;
import com.phytech.model.ProjectMetadata;
import com.phytech.utils.Constants;
import com.phytech.utils.PhytechApi;
import com.phytech.utils.PropertiesManager;

import geodesy.UTM_Point;
import ndviToolBox.model.Image;
import ndviToolBox.model.PeekAlert;
//import ndviToolBox.model.RGB_Image;

public class PeekDetector {
	private static final Logger log = Logger.getLogger(PeekDetector.class);
	private static final int pixelResolution = 10;
	private static boolean DEBUG = false;
	private static boolean inited = false;
	private static double ndviPeekDiffrence = Constants.DEFAULT_NDVI_PEEK_DIFFERENCE;
	private static double minimalNDVIAverageToTesstPeek = Constants.DEFAULT_MINIMAL_NDVI_AVERAGE_TO_REST_PEEK;

	public static void main(String[] args) {
		PropertiesManager.initConfiguration(System.getenv(Constants.CONFIGURATION_FILE_VARIABLE_NAME));

		String oldKml = "C:\\Users\\Misha\\Documents\\Debug\\52251-2019-05-13.kml";
		String rgbOld = "C:\\Users\\Misha\\Documents\\Debug\\52251-2019-05-13_rgb.kml";

		String newKml = "C:\\Users\\Misha\\Documents\\Debug\\52251-2019-05-30.kml";
		String rgbNew = "C:\\Users\\Misha\\Documents\\Debug\\52251-2019-05-30_rgb.kml";

		ProjectMetadata pmd = PhytechApi.getProjectMetaData(40369);

		PeekAlert alert = new PeekAlert();
		if (isPeekDetected(oldKml, rgbOld, newKml, rgbNew, pmd)) {
			alert.setData(true);
			alert.setPlotId(pmd.getPlotId());
			alert.setDate(System.currentTimeMillis());
			alert.setResolution(pixelResolution);
		}

		System.out.println(alert.toJson());

		System.out.println("Done");
	}

	private static void initParams() {

		if (System.getenv(Constants.MINIMAL_AVERAGE_NDVI_TO_TEST_PEEK_ENV_VARIABLE_NAME) != null) {
			minimalNDVIAverageToTesstPeek = Double
					.parseDouble(System.getenv(Constants.MINIMAL_AVERAGE_NDVI_TO_TEST_PEEK_ENV_VARIABLE_NAME));
		}
		if (System.getenv(Constants.NDVI_PEEK_DIFFERENCE_ENV_VARIABLE_NAME) != null) {
			ndviPeekDiffrence = Double.parseDouble(System.getenv(Constants.NDVI_PEEK_DIFFERENCE_ENV_VARIABLE_NAME));
		}

		log.info("Minimal average NDVI to test peek " + minimalNDVIAverageToTesstPeek);
		log.info("NDVI diffrenece to peek " + minimalNDVIAverageToTesstPeek);

	}

	public static boolean locateLowGrowthPixels(String oldNDVI, String oldRGB, String newNDVI, String newRGB,
			int projectId) {
		ProjectMetadata pmd = PhytechApi.getProjectMetaData(projectId);
		return isPeekDetected(oldNDVI, oldRGB, newNDVI, newRGB, pmd);
	}

	public static boolean isPeekDetected(String oldNDVI, String oldRGB, String newNDVI, String newRGB,
			ProjectMetadata pmd) {

		boolean detected = false;

		if (System.getenv("DEBUG") != null) {
			DEBUG = Boolean.parseBoolean(System.getenv("DEBUG"));
		}

		if (pmd == null) {
			return detected;
		}

		if (!inited) {
			initParams();
			inited = true;
		}

		log.debug("Old NDVI " + oldNDVI + " Old RGB " + oldRGB + " New NDVI " + newNDVI + " New RGB " + newRGB
				+ " Crop " + pmd.getCropType());

		File ndviFileOld = new File(oldNDVI);
		File ndviFileNew = new File(newNDVI);

		Image imageOld = new Image();
		Image imageNew = new Image();

		// parse 2 NDVI files
		try {
			if (oldNDVI != null && ndviFileOld.exists()) {
				imageOld.parse(ndviFileOld);
			} else {
				log.error("Old file doesn't exists");
				return detected;

			}

			if (newNDVI != null && ndviFileNew.exists()) {
				imageNew.parse(ndviFileNew);
			} else {
				log.error("New file doesn't exists");
				return detected;

			}
		} catch (Exception e) {
			log.error("Error during kml parser");
			return detected;

		}

		if (imageOld.getPixels().isEmpty() || imageOld.getPixels().isEmpty()) {
			log.error("Empty file");
			return detected;
		}

		// Fix possible difference in MBR between old and new NDVI
		UTM_Point commonMin = new UTM_Point(Math.max(imageOld.getMin().getEasting(), imageNew.getMin().getEasting()),
				Math.max(imageOld.getMin().getNorthing(), imageNew.getMin().getNorthing()), imageNew.getMin().getZone(),
				imageNew.getMin().getHemisphere());

		UTM_Point commonMax = new UTM_Point(Math.min(imageOld.getMax().getEasting(), imageNew.getMax().getEasting()),
				Math.min(imageOld.getMax().getNorthing(), imageNew.getMax().getNorthing()), imageNew.getMax().getZone(),
				imageNew.getMax().getHemisphere());

		imageOld.limitBoundary(commonMin, commonMax);
		imageNew.limitBoundary(commonMin, commonMax);

		if (DEBUG) {
			log.debug("Boundaries: " + " LB: " + commonMin.toJson() + " RT: " + commonMax.toJson());
			log.debug("old image dimensions " + imageOld.numX() + "x" + imageOld.numY());
			log.debug("new image dimensions " + imageNew.numX() + "x" + imageNew.numY());
		}

		// clean external borders
		SobelEdgeDetector extSobel = new SobelEdgeDetector();
		extSobel.process(imageOld);

		if (DEBUG) {
			extSobel.ImageWrite("C:\\Temp\\DirOld.png", extSobel.Direction);
			extSobel.ImageWrite("C:\\Temp\\MagOld.png", extSobel.Magnitute);
		}

		// remove pixels near the edges (external borders of NDVI)
		imageOld.cleanOutBorders(extSobel);

		extSobel.process(imageNew);
		if (DEBUG) {
			extSobel.ImageWrite("C:\\Temp\\DirNew.png", extSobel.Direction);
			extSobel.ImageWrite("C:\\Temp\\MagNew.png", extSobel.Magnitute);
		}

		imageNew.cleanOutBorders(extSobel);

		// calculate statistics for both NDVI files
		imageNew.calcStatistics();
		imageOld.calcStatistics();

		// // parse 2 RGB files
		// File rgbFileOld = null;
		// OneChannelImage rImageOld = null;
		//
		// if (oldRGB != null) {
		// rgbFileOld = new File(oldRGB);
		// if (!rgbFileOld.exists()) {
		// rgbFileOld = null;
		// } else {
		// rImageOld = new OneChannelImage(Channel.R);
		// try {
		// rImageOld.parse(rgbFileOld);
		// rImageOld.cleanOutBorders(extSobel);
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		//
		// }
		// }
		// File rgbFileNew = null;
		// OneChannelImage rImageNew = null;
		// if (newRGB != null) {
		// rgbFileNew = new File(newRGB);
		// if (!rgbFileNew.exists()) {
		// rgbFileNew = null;
		// } else {
		// try {
		// rImageNew = new OneChannelImage(Channel.R);
		// rImageNew.parse(rgbFileNew);
		// rImageNew.cleanOutBorders(extSobel);
		// if (DEBUG) {
		// rImageNew.printMatrix("C:\\Temp\\");
		// }
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		//
		// }
		//
		// }
		//
		// RGB_Image rgbOld = FootPathDetector.findFootPath(rgbFileOld);
		// RGB_Image rgbNew = FootPathDetector.findFootPath(rgbFileNew);

		// int cloudPixelsNum = 0;
		// if (rImageOld != null) {
		// cloudPixelsNum += rImageOld.getCloudPixelsNumber(rgbOld);
		// }
		//
		// if (rImageNew != null) {
		// cloudPixelsNum += rImageNew.getCloudPixelsNumber(rgbNew);
		// }
		//
		// if (cloudPixelsNum > 0) {
		// log.info(cloudPixelsNum + " pixels detected as clouds. Skip");
		// return detected;
		// }

		detected = imageNew.getStatistics().getAverage() > minimalNDVIAverageToTesstPeek
				&& (imageNew.getStatistics().getAverage() - imageOld.getStatistics().getAverage() > ndviPeekDiffrence);

		return detected;
	}

}
