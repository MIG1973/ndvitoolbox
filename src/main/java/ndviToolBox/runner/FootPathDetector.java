package ndviToolBox.runner;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import ndviToolBox.model.ArrayUtils;
import ndviToolBox.model.KML_Statistics;
import ndviToolBox.model.Pixel;
import ndviToolBox.model.RGB_Image;

public class FootPathDetector {

	private static final Logger log = Logger.getLogger(FootPathDetector.class);
	private static boolean DEBUG = false;
	private static int[][] monotonicAreasMask;

	@SuppressWarnings("unused")
	public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {

		String rgbFileName = "C:\\Users\\misha\\Downloads\\28719_rgb.kml";
		String ndviFileName = "C:\\Users\\misha\\Downloads\\28719.kml";

		File rgb = new File(rgbFileName);
		File ndvi = new File(ndviFileName);

		String content = "";

		if (rgb.exists() && rgb.canRead()) {
			Scanner scanner;
			try {
				scanner = new Scanner(rgb);
				content = scanner.useDelimiter("\\Z").next();
			} catch (FileNotFoundException e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}
		}

		findFootPath(rgb);

		System.currentTimeMillis();

	}

	public static RGB_Image findFootPath(File rgb) {

		if (System.getenv("DEBUG") != null) {
			DEBUG = Boolean.parseBoolean(System.getenv("DEBUG"));
		}

		// parse RGB file
		RGB_Image image = new RGB_Image();
		try {
			if (rgb != null) {
				image.parse(rgb);
			} else {
				return null;
			}

		} catch (Exception e) {
			log.debug("Error during kml parser. No suggestion");
			return null;
		}

		image = cleanPathes(image);

		return image;

	}

	public static RGB_Image cleanPathes(RGB_Image image) {

		// Sobel operator to remove external borders
		SobelEdgeDetector extSobel = new SobelEdgeDetector();
		extSobel.process(image);

		if (DEBUG) {
			extSobel.ImageWrite("C:\\Temp\\DirectionRGBExt.png", extSobel.Direction);
			extSobel.ImageWrite("C:\\Temp\\MagnituteRGBExt.png", extSobel.Magnitute);
		}
		image.cleanOutBorders(extSobel, 0);

		int numX = image.numX();
		int numY = image.numY();

		// binary mask. 0 - for "gray" pixels such as roads, paths etc.
		monotonicAreasMask = new int[numX][numY];

		image.calcStatistics();
		KML_Statistics statistics = image.getStatistics();

		for (int i = 0; i < numX; i++) {
			for (int j = 0; j < numY; j++) {

				Pixel pix = image.getMatrix()[i][j];
				if (pix != null && pix.getValue() != null) {
					if (Math.abs(pix.getValue() - statistics.getMedian()) < 3.0 * statistics.getMAD()) {
						monotonicAreasMask[i][j] = 1;
					}
				}
			}
		}
		if (DEBUG) {
			ArrayUtils.printMatrix("Areas marked as roads/foot-paths etc.",monotonicAreasMask);
		}

		// apply this mask to source image, so "gray" pixels became null
		image.applyMask(monotonicAreasMask);

		if (DEBUG) {

			// Sobel operator to clean out internal edges
			SobelEdgeDetector intSobel = new SobelEdgeDetector();
			intSobel.process(image);

			if (DEBUG) {
				intSobel.ImageWrite("C:\\Temp\\DirectionRGBInternal.png", intSobel.Direction);
				intSobel.ImageWrite("C:\\Temp\\MagnituteRGBInternal.png", intSobel.Magnitute);
			}

			// clean foot pathes
			// image.cleanOutBorders(intSobel,0);
		}

		log.debug(image.numX() + "x" + image.numY());

		return image;
	}

	public static void saveToImage(Pixel[][] data, String path) {
		BufferedImage res = new BufferedImage(data.length, data[0].length, BufferedImage.TYPE_INT_RGB);

		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[0].length; j++) {
				if (data[i][j] != null && data[i][j].getValue() != null && data[i][j].getValue() > 0) {
					res.setRGB(i, j, Color.BLACK.getRGB());
				} else {
					res.setRGB(i, j, Color.WHITE.getRGB());
				}

			}
		}

		RenderedImage rendImage = res;
		try {
			ImageIO.write(rendImage, "bmp", new File(path));
		} catch (IOException e) {
			log.error("Error when rendering image", e);
		}

	}

}
