package ndviToolBox.model;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.random.EmpiricalDistribution;

public class KML_Statistics {
	private Double MAD;
	public DescriptiveStatistics descriptiveStatistics;

	public KML_Statistics() {
		descriptiveStatistics = new DescriptiveStatistics();
	}

	public Double getSTDEV() {
		return descriptiveStatistics.getStandardDeviation();
	}

	public Double getMAD() {
		return MAD;
	}

	public void setMAD(Double MAD) {
		this.MAD = MAD;
	}

	public Double getMedian() {
		return descriptiveStatistics.getPercentile(50.0);
	}

	public Double getAverage() {
		return descriptiveStatistics.getMean();
	}

	public long getPixelsNum() {
		return descriptiveStatistics.getN();
	}

	@Override
	public String toString() {
		String str = "median:" + getMedian() + ",MAD:" + getMAD() + ",STD:" + getSTDEV() + ",average:" + getAverage()
				+ ",#:" + getPixelsNum();
		return str;
	}

	public int binNo() {
		int binsNumber = 0;
		double q75 = descriptiveStatistics.getPercentile(75.0);
		double q25 = descriptiveStatistics.getPercentile(25.0);
		long N = descriptiveStatistics.getN();
		double binSize = (2.0 * (q75 - q25) / Math.pow(N, 1.0 / 3.0));

		binsNumber = (int) ((descriptiveStatistics.getMax() - descriptiveStatistics.getMin()) / binSize) + 1;

		return Math.max(binsNumber, 30);
	}

	public void addValue(Double value) {
		descriptiveStatistics.addValue(value);
	}

	public long[] getBinPropabilities(int nBins) {
		long[] histogram = new long[nBins];

		EmpiricalDistribution distribution = new EmpiricalDistribution(nBins);
		distribution.load(descriptiveStatistics.getValues());
		int k = 0;
		for (org.apache.commons.math3.stat.descriptive.SummaryStatistics stats : distribution.getBinStats()) {
			histogram[k++] = stats.getN();
		}

		return histogram;
	}

	public double[] getBinValues(int nBins) {
		double[] values = new double[nBins + 1];

		double range = descriptiveStatistics.getMax() - descriptiveStatistics.getMin();
		double step = range / nBins;

		for (int k = 0; k < values.length; k++) {
			values[k] = descriptiveStatistics.getMin() + k * step;
		}

		return values;
	}
}
