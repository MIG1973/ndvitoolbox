package ndviToolBox.model;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import geodesy.UTM_Point;

public class RGB_Image extends Image {
	protected ImageType type = ImageType.ONE_CHANNEL;

	@Override
	protected void parseDocument(Document document) {
		// Normalize the XML Structure; It's just too important !!
		document.getDocumentElement().normalize();

		int pointId = 0;

		// Get all Placemark nodes
		NodeList nList = document.getElementsByTagName("Placemark");

		// run through placemarks and parse each one to utm point
		for (int temp = 0; temp < nList.getLength(); temp++) {

			Node nNode = nList.item(temp);

			if (nNode.getNodeType() == Node.ELEMENT_NODE && nNode.getNodeName() == "Placemark") {

				Node multiGeometry = getInnerNode(nNode.getChildNodes(), "MultiGeometry");

				Node value = getInnerNode(nNode.getChildNodes(), "name");
				Double val = null;
				if (value != null) {
					val = parseValue(value);
				}

				Node polygon = getInnerNode(multiGeometry.getChildNodes(), "Polygon");
				Node outerBoundaryIs = getInnerNode(polygon.getChildNodes(), "outerBoundaryIs");
				Node linearRing = getInnerNode(outerBoundaryIs.getChildNodes(), "LinearRing");
				Node coordinates = getInnerNode(linearRing.getChildNodes(), "coordinates");

				UTM_Point utmPoint = null;
				if (coordinates != null) {
					String coordinateStr = coordinates.getTextContent();
					utmPoint = parseCoordinatesString(coordinateStr);

					double easting = utmPoint.getEasting();
					double northing = utmPoint.getNorthing();

					if (easting < min.getEasting()) {
						min.setEasting(easting);
					}

					if (easting > max.getEasting()) {
						max.setEasting(easting);
					}

					if (northing < min.getNorthing()) {
						min.setNorthing(northing);
					}

					if (northing > max.getNorthing()) {
						max.setNorthing(northing);
					}

				}

				Pixel pixel = new Pixel();
				pixel.setCoordinate(utmPoint);
				pixel.setValue(val);
				pixel.setId(pointId);

				pixels.add(pixel);

				pointId++;
			}

			values = new double[pixels.size()];
			int index = 0;
			for (Pixel px : pixels) {
				values[index++] = px.getValue();
			}
			if (!pixels.isEmpty()) {
				min.setZone(pixels.get(0).getCoordinate().getZone());
				min.setHemisphere(pixels.get(0).getCoordinate().getHemisphere());
				max.setZone(pixels.get(0).getCoordinate().getZone());
				max.setHemisphere(pixels.get(0).getCoordinate().getHemisphere());

			}

		}

	}

	protected Double parseValue(Node value) {
		Double r = null;
		Double g = null;
		Double b = null;
		Double val = null;

		String sTemp = value.getTextContent();
		String[] rgb = sTemp.split(" ");
		if (rgb.length == 3) {
			r = Double.parseDouble(rgb[0].split(":")[1]);
			g = Double.parseDouble(rgb[1].split(":")[1]);
			b = Double.parseDouble(rgb[2].split(":")[1]);

			val = (r + g + b) / 3.0;
			return val;

		}

		return null;
	}

	public boolean isPixelOnPath(int r, int c) {
		return isPixelOnPath(r, c, 1);
	}

	public boolean isPixelOnPath(int r, int c, int radius) {
		for (int i = Math.max(0, r - radius); i <= Math.min(matrix.length - 1, r + radius); i++) {
			for (int j = Math.max(0, c - radius); j <= Math.min(matrix[i].length - 1, c + radius); j++) {
				if (matrix[i][j] == null) {
					return true;
				}
			}
		}

		return false;
	}

	public void applyMask(int[][] mask) {
		pixels.clear();
		for (int r = 0; r < matrix.length; r++) {
			for (int c = 0; c < matrix[0].length; c++) {

				if (mask[r][c] == 0) {
					matrix[r][c] = null;
				} else {
					if (matrix[r][c] != null) {
						pixels.add(matrix[r][c]);

					}

				}

				values = new double[pixels.size()];
				int index = 0;
				for (Pixel px : pixels) {
					values[index++] = px.getValue();
				}
			}
		}

	}
}
