package ndviToolBox.runner;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.phytech.database.DBConnectionPoolManager;
import com.phytech.database.EnumDBType;
import com.phytech.model.Crop;
import com.phytech.model.ProjectMetadata;
import com.phytech.model.Enumerations.CropType;
import com.phytech.utils.Constants;
import com.phytech.utils.CropsFactory;
import com.phytech.utils.PhytechApi;
import com.phytech.utils.PropertiesManager;

import geodesy.GeoConvertor;
import geodesy.Geo_Point;
import geodesy.UTM_Point;
import ndviToolBox.model.ArrayUtils;
import ndviToolBox.model.Channel;
import ndviToolBox.model.ClusterManager;
import ndviToolBox.model.ManagementZonesAlert;
import ndviToolBox.model.ManagementZonesAlertData;
import ndviToolBox.model.Image;
import ndviToolBox.model.OneChannelImage;
import ndviToolBox.model.KML_Statistics;
import ndviToolBox.model.GroupOfPixel;
import ndviToolBox.model.Pixel;
import ndviToolBox.model.Point;
import ndviToolBox.model.RGB_Image;
import ndviToolBox.model.SuggestedLocation;

public class ManagementZonesFinder {

	private static final Logger log = Logger.getLogger(ManagementZonesFinder.class);
	private static final int pixelResolution = 10;

	private static boolean DEBUG = false;
	private static boolean BATCH_PROCESSING = false;
	private static boolean GENERATE_FULL_OUTPUT = false;
	private static String OUTPUT_FOLDER = "C:\\Users\\misha\\Documents\\MZ test 24-07\\New folder\\";

	public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
		PropertiesManager.init("");
		if (System.getenv("BATCH_PROCESSING") != null) {
			BATCH_PROCESSING = Boolean.parseBoolean(System.getenv("BATCH_PROCESSING"));
		}

		if (BATCH_PROCESSING) {
			batchProcess();
			return;
		}

		// One NDVI file processing
		String sourceKmlFile = OUTPUT_FOLDER + "36546-2018-07-18.kml";
		File ndviFile = new File(sourceKmlFile);

		File rgb_file = new File(OUTPUT_FOLDER + "36546-2018-07-18_rgb.kml");

		if (!ndviFile.exists() || !ndviFile.canRead()) {
			ndviFile = null;
		}

		if (!rgb_file.exists() || !rgb_file.canRead()) {
			rgb_file = null;
		}

		GENERATE_FULL_OUTPUT = true;

		ManagementZonesAlert alert = findManagementZones(ndviFile, rgb_file,
				CropsFactory.getInstance().getCrop(CropType.SOYBEAN), false, 23800);

		ManagementZonesAlertData alertData = (ManagementZonesAlertData) alert.getData();

		log.debug("Management groups number: " + alertData.getGroups().size());

	}

	private static void batchProcess() {

		String batchFolder = System.getenv("BATCH_FOLDER");
		if (batchFolder == null) {
			log.debug("Invalid batch folder");
			return;
		}

		File root = new File(batchFolder);
		if (!root.exists()) {
			log.debug("Batch folder doesn't exists");
			return;
		}

		boolean force = false;
		if (System.getenv("FORCE") != null) {
			force = Boolean.parseBoolean(System.getenv("FORCE"));
		}

		DBConnectionPoolManager.createPoolForDB(EnumDBType.JAVA);
		// PhytechApi.initAllProjectsMetadata();

		for (File subFolder : root.listFiles()) {

			File workingFolder;
			if (subFolder.getName().contains("out")) {
				continue;
			}

			if (subFolder.isDirectory()) {
				workingFolder = subFolder;
			} else {
				workingFolder = root;
			}
			GENERATE_FULL_OUTPUT = true;
			OUTPUT_FOLDER = workingFolder.getPath() + "\\out\\";
			File outputDir = new File(OUTPUT_FOLDER);
			if (outputDir.exists()) {
				outputDir.delete();
			}
			outputDir.mkdirs();
			for (File f : workingFolder.listFiles()) {
				if (!f.getName().endsWith(".kml") || f.isDirectory() || f.getName().contains("_rgb.kml")) {
					continue;
				}
				log.debug("Processing " + f.getName());
				ProjectMetadata pmd = PhytechApi.getProjectMetaData(getIDByName(f.getName()));
				if (pmd == null) {
					log.warn("Project metadata not found. Skip");
					continue;
				}
				ManagementZonesAlert alert = findManagementZones(f, new File(NDVIToRGBFileName(f.getAbsolutePath())),
						pmd.getCrop(), true, pmd.getPlotId(), force);

				ManagementZonesAlertData alertData = (ManagementZonesAlertData) alert.getData();
				log.debug("Management groups number: " + alertData.getGroups().size());
			}
		}
		log.debug("Done");
	}

	private static Integer getIDByName(String name) {
		String[] parts = name.split("-");
		return Integer.parseInt(parts[0]);
	}

	public static ManagementZonesAlert findManagementZones(File ndviFile, File rgbFile, Crop crop, boolean isPivot,
			Integer plotId) {
		return findManagementZones(ndviFile, rgbFile, crop, isPivot, plotId, false);
	}

	/**
	 * Test given KML for different management zones
	 * 
	 * @param content
	 *            NDVI KML file
	 * @param pmd
	 *            - project metadata
	 * @return
	 */
	public static ManagementZonesAlert findManagementZones(File ndviFile, File rgbFile, Crop crop, boolean isPivot,
			Integer plotId, boolean force) {
		log.info("Start...");
		ManagementZonesAlert res = new ManagementZonesAlert();
		ManagementZonesAlertData alertData = new ManagementZonesAlertData();

		// parse NDVI KML file
		Image ndviImage = new Image();
		try {
			if (ndviFile != null) {
				ndviImage.parse(ndviFile);
			}
		} catch (Exception e) {
			log.debug("Error during NFVI kml parser. No suggestion");
			res.setData(alertData);
			return res;
		}

		if (force) {
			return findManagementZonesForce(ndviImage, rgbFile, crop, isPivot, plotId);

		} else {

			return findManagementZones(ndviImage, rgbFile, crop, isPivot, plotId);
		}
	}

	private static ManagementZonesAlert findManagementZones(Image ndviImage, File rgbFile, Crop crop, boolean isPivot,
			Integer plotId) {
		return findManagementZones(ndviImage, rgbFile, crop, isPivot, plotId, Constants.MINIMAL_M_VALUE,
				Constants.MINIMAL_DISTANCE_BETWEEN_HIST_PEAKS);
	}

	private static ManagementZonesAlert findManagementZonesForce(Image ndviImage, File rgbFile, Crop crop,
			boolean isPivot, Integer plotId) {
		return findManagementZones(ndviImage, rgbFile, crop, isPivot, plotId, -1.0, -1.0);
	}

	private static ManagementZonesAlert findManagementZones(Image ndviImage, File rgbFile, Crop crop, boolean isPivot,
			Integer plotId, Double mValueLimit, Double peaksDistanceLimit) {
		initProperties();

		ManagementZonesAlert res = new ManagementZonesAlert();
		ManagementZonesAlertData alertData = new ManagementZonesAlertData();

		alertData.setDate(System.currentTimeMillis());
		alertData.setResolution(pixelResolution);

		// original image dimensions
		int nRow = ndviImage.matrix.length;
		int nCol = ndviImage.matrix[0].length;

		// clean out external borders. Edges are cleaned with radius = 1
		SobelEdgeDetector sobel = new SobelEdgeDetector();
		sobel.process(ndviImage);
		if (DEBUG) {
			sobel.ImageWrite("C:\\Temp\\DirExternalManagementZones.png", sobel.Direction);
			sobel.ImageWrite("C:\\Temp\\MagExternalManagementZones.png", sobel.Magnitute);
		}
		// clean borders from all 3 images
		ndviImage.cleanOutBorders(sobel);

		RGB_Image rgbImage = FootPathDetector.findFootPath(rgbFile);
		OneChannelImage rImage;
		int numberOfCloudPixels = 0;
		if (rgbImage != null) {
			rImage = new OneChannelImage(Channel.R);
			try {
				rImage.parse(rgbFile);
				rImage.cleanOutBorders(sobel);
				numberOfCloudPixels = rImage.getCloudPixelsNumber(rgbImage);

			} catch (ParserConfigurationException | SAXException | IOException e) {
				rImage = null;
			}
		}

		if (numberOfCloudPixels > 0) {
			log.warn(numberOfCloudPixels + " pixels detected as clouds. Can't perform management zones analysis");
			res.setData(alertData);
			return res;
		}

		// clean out foot path pixels from the NDVI image
		if (rgbImage != null) {
			ndviImage.cleanFootPathPixels(rgbImage);
		}

		// calculate original image statistics
		ndviImage.calcStatistics();
		KML_Statistics stat = ndviImage.getStatistics();
		int binsNumber = stat.binNo();
		long[] binProps = stat.getBinPropabilities(binsNumber);
		double[] binVals = stat.getBinValues(binsNumber);
		double bandWidth = binVals[1] - binVals[0];

		// Test for the feasibility of finding groups:

		// M-Value test
		double mValue = calcMValue(binProps);
		log.info("mValue: " + mValue);
		if (mValue <= mValueLimit) {
			log.info("below the threshold [" + mValueLimit + "]");
			res.setData(alertData);
			return res;
		}

		// Distance between the 2 highest peaks of histogram test
		double distanceBetweenPeaks = calcDistanceBetweenPeaks(ndviImage.getValues(), bandWidth,
				stat.descriptiveStatistics.getMin(), stat.descriptiveStatistics.getMax());
		log.info("distance between peaks: " + distanceBetweenPeaks);

		if (distanceBetweenPeaks < peaksDistanceLimit) {
			log.info("below the threshold [" + peaksDistanceLimit + "]");
			res.setData(alertData);
			return res;
		}

		// Kmeans clustering section

		ClusterManager cm = getClusterManager(ndviImage, plotId);

		// prepare clustered matrixes

		// label number for each pixel, 0 for non exists pixels
		int[][] clusteredMatrix = new int[nRow][nCol];

		// 1 for exists pixels, 0 otherwise
		int[][] binaryClusteredMatrix = new int[nRow][nCol];

		for (int r = 0; r < nRow; r++) {
			for (int c = 0; c < nCol; c++) {
				if (ndviImage.matrix[r][c] != null) {
					Integer clusterNum = cm.getCluster(ndviImage.matrix[r][c].getId());
					if (clusterNum != null) {
						clusteredMatrix[r][c] = clusterNum + 1;
						binaryClusteredMatrix[r][c] = 1;
					}
				}
			}
		}

		if (DEBUG) {
			ArrayUtils.printMatrix("clustered matrix", clusteredMatrix);
		}

		// clean out internal borders
		sobel.process(clusteredMatrix);
		if (DEBUG) {
			sobel.ImageWrite("C:\\Temp\\DirInternalManagementZones.png", sobel.Direction);
			sobel.ImageWrite("C:\\Temp\\MagInternalManagementZones.png", sobel.Magnitute);
		}

		// Results of Sobel operator - 0 for empty pixel or border, 1 otherwise
		int[][] sob = hypotenuseInvt(sobel.Gx, sobel.Gy);

		int[][] spotsMatrix = ArrayUtils.mult(binaryClusteredMatrix, sob);

		if (DEBUG) {
			ArrayUtils.printMatrix("spots matrix", spotsMatrix);
		}

		Map<Integer, List<Point>> labels = ImageSegmentator.labelImageToMap(ndviImage.numX() * ndviImage.numY(),
				spotsMatrix);

		int[] maxLabelPerSpot = getMaxLabelPerSpot(cm, labels, clusteredMatrix);

		// prepare different "NDVI images" for the biggest label of each patch and look
		// for suggested location for each one of them separately

		for (int i = 1; i <= cm.getClustersNumber(); i++) {
			Image patchImage = new Image();
			patchImage.matrix = new Pixel[nRow][nCol];

			List<Point> currentLabel = labels.get(maxLabelPerSpot[i]);

			if (currentLabel == null) {
				log.info("Maximal label is too small. Skip");
				continue;
			}

			for (Point p : currentLabel) {
				Pixel px = ndviImage.matrix[p.getI()][p.getJ()];
				patchImage.matrix[p.getI()][p.getJ()] = px;

			}
			SuggestedLocation suggestedLoc = NonPivotLocationFinder.findLocationForManagementZones(patchImage);
			alertData.addLocation(suggestedLoc);
		}

		for (int i = 1; i <= cm.getClustersNumber(); i++) {
			List<Point> currentLabel = labels.get(maxLabelPerSpot[i]);
			if (currentLabel == null) {
				continue;
			}

			log.debug("Label " + maxLabelPerSpot[i] + " size " + currentLabel.size());
			GroupOfPixel currentGroup = new GroupOfPixel();
			currentGroup.setGroupId(maxLabelPerSpot[i]);
			currentGroup.setSize(currentLabel.size());
			for (Point p : currentLabel) {
				UTM_Point utm = ndviImage.matrix[p.getI()][p.getJ()].getCoordinate();
				Geo_Point geo = GeoConvertor.getInstance().UTM2Deg(utm);
				currentGroup.addPoint(geo);
			}

			if (DEBUG) {
				Utils.saveGeoPointsToKML(currentGroup.getPixels(),
						OUTPUT_FOLDER + "_Group_" + currentGroup.getGroup_id() + ".kml");
			}
			alertData.addGroup(currentGroup);
		}

		res.setData(alertData);
		return res;

	}

	private static int[] getMaxLabelPerSpot(ClusterManager cm, Map<Integer, List<Point>> labels,
			int[][] clusteredMatrix) {
		int[] maxLabelPerSpot = new int[cm.getClustersNumber() + 1];
		int[] maxLabelPerSpotSize = new int[cm.getClustersNumber() + 1];

		for (Integer label : labels.keySet()) {
			int labelSize = labels.get(label).size();
			if (labelSize > PropertiesManager.min_spot_size) {
				int spotNumber = clusteredMatrix[labels.get(label).get(0).getI()][labels.get(label).get(0).getJ()];
				if (labelSize > maxLabelPerSpotSize[spotNumber]) {
					maxLabelPerSpotSize[spotNumber] = labelSize;
					maxLabelPerSpot[spotNumber] = label;
				}
			}
		}

		return maxLabelPerSpot;
	}

	private static ClusterManager getClusterManager(Image originalImage, Integer plotId) {
		originalImage.calcPairwiseDistances();

		// Split image to 2,or 3 clusters and calculate the "Silhouette Score" for
		// each clustering result
		double[] silhouette_score = new double[5];

		for (int j = 2; j < 4; j++) {
			KMeansPlusPlusClusterer<Pixel> clusterer = new KMeansPlusPlusClusterer<Pixel>(j, 300);
			List<CentroidCluster<Pixel>> clusterResults = clusterer.cluster(originalImage.getPixels());

			if (DEBUG) {
				for (int i = 0; i < clusterResults.size(); i++) {
					System.out.println("Cluster " + i + " Points # " + clusterResults.get(i).getPoints().size());
					System.out.println("Center is " + clusterResults.get(i).getCenter().getPoint()[0]);
				}
				System.out.println("===============================================");
			}
			silhouette_score[j] = calcSilhouetteScore(originalImage, clusterResults);
		}

		// Choose the bust clustering strategy
		int bestClusteringStrategy = 0;
		double maxSlhouetteScore = silhouette_score[0];

		for (int i = 1; i < silhouette_score.length; i++) {
			if (silhouette_score[i] > maxSlhouetteScore) {
				maxSlhouetteScore = silhouette_score[i];
				bestClusteringStrategy = i;
			}
		}

		KMeansPlusPlusClusterer<Pixel> bestClusterer = new KMeansPlusPlusClusterer<Pixel>(bestClusteringStrategy, 300);
		List<CentroidCluster<Pixel>> bestResults = bestClusterer.cluster(originalImage.getPixels());

		ClusterManager cm = new ClusterManager(bestResults);
		cm.setClustersNumber(bestClusteringStrategy);

		if (GENERATE_FULL_OUTPUT) {
			String id = "";
			if (plotId != null) {
				id = plotId.toString();
			}
			for (int i = 0; i < bestResults.size(); i++) {
				double clusterNdviMean = getNDVIMean(bestResults.get(i).getPoints());
				int styleNo = 0;
				if (bestResults.size() == 2) {
					if (i == 0) {
						styleNo = 1;
					} else {
						styleNo = 5;
					}

				}

				if (bestResults.size() == 3) {
					if (i == 0) {
						styleNo = 1;
					} else if (i == 1) {
						styleNo = 3;
					} else {
						styleNo = 5;
					}

				}
				Utils.savePixelsToKML(bestResults.get(i).getPoints(),
						OUTPUT_FOLDER + id + "_cluster_" + clusterNdviMean + ".kml", styleNo);
			}
		}

		return cm;
	}

	private static void initProperties() {
		if (System.getenv("DEBUG") != null) {
			DEBUG = Boolean.parseBoolean(System.getenv("DEBUG"));
		}

		if (System.getenv("GENERATE_FULL_OUTPUT") != null) {
			GENERATE_FULL_OUTPUT = Boolean.parseBoolean(System.getenv("GENERATE_FULL_OUTPUT"));
		}

		if (System.getenv("OUTPUT_FOLDER") != null) {
			OUTPUT_FOLDER = System.getenv("OUTPUT_FOLDER");
		}
	}

	private static double calcSilhouetteScore(Image image, List<CentroidCluster<Pixel>> clusterResults) {

		// double[] pixelScores = new double[image.maxIndex+1];
		double sum = 0.0;
		int pixelsCounter = 0;

		// for (CentroidCluster<NDVI_Pixel> cluster:clusterResults) {
		for (int i = 0; i < clusterResults.size(); i++) {
			CentroidCluster<Pixel> cluster = clusterResults.get(i);
			for (Pixel pixel : cluster.getPoints()) {
				double ai = getSScore(image, pixel, cluster.getPoints(), false);
				double bi = Double.MAX_VALUE;

				for (CentroidCluster<Pixel> cluster2 : clusterResults) {
					if (cluster2.getCenter().getPoint()[0] == cluster.getCenter().getPoint()[0]) {
						continue;
					}

					double tmp = getSScore(image, pixel, cluster2.getPoints(), true);
					if (tmp < bi) {
						bi = tmp;
					}
				}
				double sc = silhouetteCoefficient(ai, bi);
				// pixelScores[pixel.getId()] = sc;
				pixelsCounter++;
				sum += sc;
			}
		}
		if (pixelsCounter == 0) {
			return 0;
		}

		return sum / pixelsCounter;
	}

	static double getSScore(Image image, Pixel px, List<Pixel> cluster, boolean selfIncluded) {
		DescriptiveStatistics ds = new DescriptiveStatistics();

		for (Pixel p : cluster) {
			ds.addValue(image.getDistances()[px.getId()][p.getId()]);
		}

		if (selfIncluded) {
			return ds.getMean();
		} else {
			return ds.getSum() / (ds.getValues().length - 1);
		}
	}

	static double getNDVIMean(List<Pixel> cluster) {
		DescriptiveStatistics ds = new DescriptiveStatistics();

		for (Pixel p : cluster) {
			ds.addValue(p.getValue());
		}

		return ds.getMean();
	}

	static double silhouetteCoefficient(double ai, double bi) {
		if (ai < bi) {
			return 1.0 - (ai / bi);
		}
		if (ai > bi) {
			return (bi / ai) - 1.0;
		}
		return 0.0;
	}

	private static double calcMValue(long[] bins) {
		double sum = 0.0;
		long maxBin = bins[0];
		for (int index = 1; index < bins.length; index++) {
			sum += Math.abs(bins[index] - bins[index - 1]);
			if (bins[index] > maxBin) {
				maxBin = bins[index];
			}
		}
		if (maxBin != 0) {
			return sum / maxBin;
		} else {
			return 0.0;
		}
	}

	private static double calcDistanceBetweenPeaks(double[] values, double bandWidth, double min, double max) {
		smile.stat.distribution.KernelDensity kd = new smile.stat.distribution.KernelDensity(values, bandWidth);

		int length = 1000;
		double[] x = new double[length];
		double[] logProb = new double[length];
		double[] kernel = new double[length];
		for (int i = 0; i < length; i++) {
			x[i] = min + i * (max - min) / (length - 1);
			logProb[i] = kd.logp(x[i]);
			kernel[i] = Math.exp(logProb[i]);
		}

		List<Map<Integer, Double>> peaks = ManagementZonesFinder.peak_detection(kernel, 0.00001);

		Map<Integer, Double> localMaxima = peaks.get(0);

		Double max1 = null, max2 = null;
		Integer max1Index = null, max2Index = null;

		for (Integer currentIndex : localMaxima.keySet()) {
			if (max1 == null) {
				max1Index = currentIndex;
				max1 = localMaxima.get(currentIndex);
				continue;
			}

			if (max2 == null) {
				max2Index = currentIndex;
				max2 = localMaxima.get(currentIndex);

				if (max1 < max2) {
					// swap
					double tmp = max2;
					max2 = max1;
					max1 = tmp;

					int tmpIndex = max2Index;
					max2Index = max1Index;
					max1Index = tmpIndex;
				}

				continue;
			}

			// we have max1 >= max2;
			double currentValue = localMaxima.get(currentIndex);

			if (currentValue > max1) {
				max2Index = max1Index;
				max1Index = currentIndex;

				max2 = max1;
				max1 = currentValue;
				continue;
			}

			if (currentValue > max2) {
				max2Index = currentIndex;
				max2 = currentValue;
				continue;

			}

		}

		if (max1Index != null && max2Index != null) {
			return Math.abs(x[max1Index] - x[max2Index]);
		} else {
			return 0.0;
		}

	}

	/**
	 * Detects peaks (calculates local minimum and maximum) in the vector
	 * <code>values</code>. The resulting list contains maximum at the first
	 * position and minimum at the last one.
	 * 
	 * Maximum and minimum maps contain the index value for a given position and the
	 * value from a corresponding vector.
	 * 
	 * A point is considered a maximum peak if it has the maximal value, and was
	 * preceded (to the left) by a value lower by <code>delta</code>.
	 * 
	 * @param values
	 *            Vector of values for whom the peaks should be detected
	 * @param delta
	 *            The proceeder of a maximum peak
	 * @param indices
	 *            Vector of indices that replace positions in resulting maps
	 * @return List of maps (maximum and minimum pairs) of detected peaks
	 */
	public static <U> List<Map<U, Double>> peak_detection(double[] values, Double delta, List<U> indices) {
		assert (indices != null);
		assert (values.length != indices.size());

		Map<U, Double> maxima = new HashMap<U, Double>();
		Map<U, Double> minima = new HashMap<U, Double>();
		List<Map<U, Double>> peaks = new ArrayList<Map<U, Double>>();
		peaks.add(maxima);
		peaks.add(minima);

		Double maximum = null;
		Double minimum = null;
		U maximumPos = null;
		U minimumPos = null;

		boolean lookForMax = true;

		Integer pos = 0;
		for (Double value : values) {
			if (maximum == null || value > maximum) {
				maximum = value;
				maximumPos = indices.get(pos);
			}

			if (minimum == null || value < minimum) {
				minimum = value;
				minimumPos = indices.get(pos);
			}

			if (lookForMax) {
				if (value < maximum - delta) {
					maxima.put(maximumPos, value);
					minimum = value;
					minimumPos = indices.get(pos);
					lookForMax = false;
				}
			} else {
				if (value > minimum + delta) {
					minima.put(minimumPos, value);
					maximum = value;
					maximumPos = indices.get(pos);
					lookForMax = true;
				}
			}

			pos++;
		}

		return peaks;
	}

	/**
	 * Detects peaks (calculates local minimum and maximum) in the vector
	 * <code>values</code>. The resulting list contains maximum at the first
	 * position and minimum at the last one.
	 * 
	 * Maximum and minimum maps contain the position for a given value and the value
	 * itself from a corresponding vector.
	 * 
	 * A point is considered a maximum peak if it has the maximal value, and was
	 * preceded (to the left) by a value lower by <code>delta</code>.
	 * 
	 * @param values
	 *            Vector of values for whom the peaks should be detected
	 * @param delta
	 *            The proceeder of a maximum peak
	 * @return List of maps (maximum and minimum pairs) of detected peaks
	 */
	public static List<Map<Integer, Double>> peak_detection(double[] values, Double delta) {
		List<Integer> indices = new ArrayList<Integer>();
		for (int i = 0; i < values.length; i++) {
			indices.add(i);
		}

		return ManagementZonesFinder.peak_detection(values, delta, indices);
	}

	private static int[][] hypotenuseInvt(double[][] x, double[][] y) {
		int[][] res = new int[x.length][x[0].length];

		for (int i = 0; i < x.length; i++) {
			for (int j = 0; j < x[i].length; j++) {
				if (Math.sqrt(Math.pow(x[i][j], 2.0) + Math.pow(y[i][j], 2.0)) > 0) {
					res[i][j] = 0;
				} else {
					res[i][j] = 1;
				}
			}
		}

		return res;
	}

	public static String NDVIToRGBFileName(String path) {
		String fileNameWithOutExt = FilenameUtils.removeExtension(path);
		return fileNameWithOutExt + "_rgb.kml";
	}
}
