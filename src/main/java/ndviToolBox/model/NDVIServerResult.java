package ndviToolBox.model;
import java.io.IOException;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;

import geodesy.Geo_Point;

public class NDVIServerResult {
	private String kmlContent;
	private Geo_Point suggestedLocation;
	private String errorMessage;

	public String getKmlContent() {
		return kmlContent;
	}

	@JsonProperty("kml_file_content")
	public void setKmlContent(String kmlContent) {
		this.kmlContent = kmlContent;
	}

	@JsonProperty("suggested_location_geo")
	public Geo_Point getSuggestedLocation() {
		return suggestedLocation;
	}

	public void setSuggestedLocation(Geo_Point suggestedLocation) {
		this.suggestedLocation = suggestedLocation;
	}

	@JsonProperty("error_message")
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	@JsonIgnore
	public String toJson() {
		String json = null;

		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return json;

	}

}
