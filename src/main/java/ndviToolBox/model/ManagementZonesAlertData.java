package ndviToolBox.model;

import java.util.ArrayList;
import java.util.List;

public class ManagementZonesAlertData extends PixelsAlertData {
	protected List<SuggestedLocation> suggestedLocations = new ArrayList<SuggestedLocation>();

	public void addLocation(SuggestedLocation loc) {
		suggestedLocations.add(loc);
	}

	public List<SuggestedLocation> getSuggestedLocations() {
		return suggestedLocations;
	}

}
