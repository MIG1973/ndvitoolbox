package ndviToolBox.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.apache.commons.math3.ml.clustering.CentroidCluster;

public class ClusterManager {
	private List<HashSet<Integer>> clusters;
	private int clustersNumber;

	public ClusterManager(List<CentroidCluster<Pixel>> centroidClusters) {
		clusters = new ArrayList<HashSet<Integer>>();

		for (int clusterNum = 0; clusterNum < centroidClusters.size(); clusterNum++) {
			CentroidCluster<Pixel> currentCluster = centroidClusters.get(clusterNum);
			HashSet<Integer> pixelsInCluster = new HashSet<>();

			for (Pixel px : currentCluster.getPoints()) {
				pixelsInCluster.add(px.getId());
			}

			clusters.add(pixelsInCluster);
		}

	}

	public Integer getCluster(int pixelId) {
		Integer cluster = null;
		for (int clusterNum = 0; clusterNum < clusters.size(); clusterNum++) {
			if (clusters.get(clusterNum).contains(pixelId)) {
				cluster = clusterNum;
				break;
			}
		}

		return cluster;
	}

	public int getClustersNumber() {
		return clustersNumber;
	}

	public void setClustersNumber(int clustersNumber) {
		this.clustersNumber = clustersNumber;
	}

}
