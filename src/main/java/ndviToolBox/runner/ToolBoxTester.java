package ndviToolBox.runner;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import ndviToolBox.model.Image;
import ndviToolBox.model.PeekAlert;

public class ToolBoxTester {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		PeekAlert alert = new PeekAlert();
		Map<String, Object> alertData = new HashMap<String, Object>();
		alertData.put("date", System.currentTimeMillis());
		alertData.put("detected", true);
		alertData.put("plot_id", 1234);
		alert.setData(alertData);

		String json = alert.toJson();
		
		System.out.println(json);
		

		convertKMLFormat();

		// testDuplicated("C:\\Users\\misha\\Downloads\\10332.kml");
		// double[] stam = new double[12];
		// stam[0] = 1.0;
		// stam[1] = 2.0;
		// stam[2] = 30.3;
		// stam[3] = 2.0;
		// stam[4] = 1.0;
		// stam[5] = 2.0;
		// stam[6] = 3.8;
		// stam[7] = 2.0;
		// stam[8] = 1.0;
		// stam[9] = 2.0;
		// stam[10] = 3.5;
		// stam[11] = 1.0;
		//
		// @SuppressWarnings("unused")
		// List<Map<Integer, Double>> peaks = ManagementZonesFinder.peak_detection(stam,
		// 1.0);

		System.out.println("Done");

	}
	


	public static void convertKMLFormat() throws ParserConfigurationException, SAXException, IOException {
		String inFolder = "C:\\Users\\misha\\Downloads\\Corn_Peek";
		String outFolder = "C:\\Users\\misha\\Downloads\\Corn_Peek_converted";

	
		File in = new File(inFolder);
		parseOneFolder(outFolder, in);

	}



	private static void parseOneFolder(String outFolder, File in) {
		File[] kmlFiles = in.listFiles();
		File out = new File(outFolder);
		if (!out.exists()) {
			out.mkdirs();
		}

		for (File f : kmlFiles) {
			System.out.println("Trying to parse " + f.getName());
			if (f.isDirectory() ) {
				parseOneFolder(outFolder + "\\" + f.getName(), f);
			}
			if (!f.getName().contains(".kml")) {
				continue;
			}
			Image image = new Image();
			try {
				image.parse(f);
			} catch (Exception ex) {
				System.out.println("Can't parse");
				continue;
			}

			ndviToolBox.runner.Utils.savePixelsToKML(image, outFolder + "\\" + f.getName());
			// geodesy.Utils.saveGeoPointsToKML1(image.pixels, outFolder + "\\" +
			// f.getName());
		}
	}

	public static void testDuplicated(String sFileName) throws ParserConfigurationException, SAXException, IOException {
		File origin = new File(sFileName);
		Image image = new Image();

		image.parse(origin);

		image.calcStatistics();
		System.out.println("Average NDVI without duplicated pixels " + image.getStatistics().getAverage());
		System.out.println(" " + image.getStatistics().getPixelsNum());

	}

}
