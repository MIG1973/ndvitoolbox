package ndviToolBox.model;

import geodesy.Geo_Point;

public class SuggestedLocation {
	private Geo_Point location;
	private SuggestedLocationStatus status;

	public Geo_Point getLocation() {
		return location;
	}

	public void setLocation(Geo_Point location) {
		this.location = location;
	}

	public SuggestedLocationStatus getStatus() {
		return status;
	}

	public void setStatus(SuggestedLocationStatus status) {
		this.status = status;
	}

}
