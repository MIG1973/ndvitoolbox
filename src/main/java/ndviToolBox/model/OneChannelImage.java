package ndviToolBox.model;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class OneChannelImage extends RGB_Image {
	private Channel channel;

	public OneChannelImage(Channel channel) {
		this.channel = channel;
		
	}
	
	@Override
	public void parse(File file) throws ParserConfigurationException, SAXException, IOException {
		super.parse(file);
		imageName+="_"+channel.name();
	}

	public Channel getChannel() {
		return channel;
	}

	@Override
	protected Double parseValue(Node value) {
		Double r = null;
		Double g = null;
		Double b = null;

		if (value != null) {
			String sTemp = value.getTextContent();
			String[] rgb = sTemp.split(" ");
			if (rgb.length == 3) {

				switch (channel) {
				case B:
					b = Double.parseDouble(rgb[2].split(":")[1]);
					return b;
				case G:
					g = Double.parseDouble(rgb[1].split(":")[1]);
					return g;
				case R:
					r = Double.parseDouble(rgb[0].split(":")[1]);
					return r;
				default:
					break;

				}

			}

		}

		return null;
	}

	public int getCloudPixelsNumber(RGB_Image pathDetector) {
		int cloudPixelsCounter = 0;
		for (int r = 0; r < numX(); r++) {
			for (int c = 0; c < numY(); c++) {
				if (matrix[r][c] != null && matrix[r][c].getValue() >= 1200 && matrix[r][c].getValue() <= 1600
						&& !pathDetector.isPixelOnPath(r, c)) {
					cloudPixelsCounter++;
				}
			}
		}

		return cloudPixelsCounter;
	}

	public boolean isCloudPixel(int r, int c, RGB_Image pathDetector) {
		if (matrix[r][c] == null) {
			return false;
		}

		if (matrix[r][c].getValue() >= 1200 && matrix[r][c].getValue() <= 1600
				&& !pathDetector.isPixelOnPath(r, c)) {
			return true;
		}

		return false;
	}
}
