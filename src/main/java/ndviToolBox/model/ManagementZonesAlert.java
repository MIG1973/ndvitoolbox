package ndviToolBox.model;

import com.phytech.model.GenericAlert;
import com.phytech.model.Enumerations.AlertType;

public class ManagementZonesAlert extends GenericAlert {

	public ManagementZonesAlert() {
		type = AlertType.MULTIPLE_AREAS_ALERT;
	}

}
