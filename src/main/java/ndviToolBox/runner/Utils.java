package ndviToolBox.runner;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

import geodesy.GeoConvertor;
import geodesy.Geo_Point;
import geodesy.UTM_Point;
import ndviToolBox.model.Image;
import ndviToolBox.model.Pixel;

public class Utils {
	private static String comma = "\"";
	private static final Logger log = Logger.getLogger(Utils.class);

	public static void saveGeoPointsToKML(List<Geo_Point> pixels, String filepath) {
		try {
			FileWriter fw = null;
			fw = new FileWriter(filepath);
			fw.write(
					"<?xml version=" + comma + 1.0 + comma + " encoding=" + comma + "UTF-8" + comma + '?' + ">" + '\n');
			fw.write("<kml xmlns=" + comma + "http://earth.google.com/kml/2.0" + comma + ">" + '\n');
			fw.write("<Document>" + '\n');
			for (Geo_Point geo : pixels) {
				fw.write("<Placemark>");
				fw.write("<Point><coordinates>" + geo.getLon() + "," + geo.getLat() + ",0" + "</coordinates></Point>");
				fw.write("</Placemark>");

			}

			fw.write("</Document>");
			fw.write("</kml>");

			fw.close();
		} catch (IOException e) {
			log.error(e);
		}
	}
	
	public static void saveGeoPointsToKML1(List<Pixel> pixels, String filepath) {
		try {
			FileWriter fw = null;
			fw = new FileWriter(filepath);
			fw.write(
					"<?xml version=" + comma + 1.0 + comma + " encoding=" + comma + "UTF-8" + comma + '?' + ">" + '\n');
			fw.write("<kml xmlns=" + comma + "http://earth.google.com/kml/2.0" + comma + ">" + '\n');
			fw.write("<Document>" + '\n');
			for (Pixel px : pixels) {
				UTM_Point utm = GeoConvertor.getInstance().Deg2UTM(px.getOriginal().get(0).getLat(), px.getOriginal().get(0).getLon());
				
				fw.write("<Placemark>");
				fw.write("<Point><coordinates>" + utm.getNorthing()+ "," + utm.getEasting()  + ",0" + "</coordinates></Point>");
				fw.write("</Placemark>");

			}

			fw.write("</Document>");
			fw.write("</kml>");

			fw.close();
		} catch (IOException e) {
			log.error(e);
		}
	}

	private static void addStyles(FileWriter fw) throws IOException {
		fw.write("<Style id=\"PolyStyle0\">" + '\n');
		fw.write("<LabelStyle>" + '\n');
		fw.write("<color>00000000</color>" + '\n');
		fw.write("<scale>0</scale>" + '\n');
		fw.write("</LabelStyle>" + '\n');
		fw.write("<LineStyle>" + '\n');
		fw.write("<color>ff0026ff</color>" + '\n');
		fw.write("<width>0.0</width>" + '\n');
		fw.write("</LineStyle>" + '\n');
		fw.write("<PolyStyle>" + '\n');
		fw.write("<color>ff0026ff</color>" + '\n');
		fw.write("</PolyStyle>" + '\n');
		fw.write("</Style>" + '\n');

		fw.write("<Style id=\"PolyStyle1\">" + '\n');
		fw.write("<LabelStyle>" + '\n');
		fw.write("<color>00000000</color>" + '\n');
		fw.write("<scale>0</scale>" + '\n');
		fw.write("</LabelStyle>" + '\n');
		fw.write("<LineStyle>" + '\n');
		fw.write("<color>ff0084ff</color>" + '\n');
		fw.write("<width>0.0</width>" + '\n');
		fw.write("</LineStyle>" + '\n');
		fw.write("<PolyStyle>" + '\n');
		fw.write("<color>ff0084ff</color>" + '\n');
		fw.write("</PolyStyle>" + '\n');
		fw.write("</Style>" + '\n');

		fw.write("<Style id=\"PolyStyle2\">" + '\n');
		fw.write("<LabelStyle>" + '\n');
		fw.write("<color>00000000</color>" + '\n');
		fw.write("<scale>0</scale>" + '\n');
		fw.write("</LabelStyle>" + '\n');
		fw.write("<LineStyle>" + '\n');
		fw.write("<color>ff00d9ff</color>" + '\n');
		fw.write("<width>0.0</width>" + '\n');
		fw.write("</LineStyle>" + '\n');
		fw.write("<PolyStyle>" + '\n');
		fw.write("<color>ff00d9ff</color>" + '\n');
		fw.write("</PolyStyle>" + '\n');
		fw.write("</Style>" + '\n');

		fw.write("<Style id=\"PolyStyle3\">" + '\n');
		fw.write("<LabelStyle>" + '\n');
		fw.write("<color>00000000</color>" + '\n');
		fw.write("<scale>0</scale>" + '\n');
		fw.write("</LabelStyle>" + '\n');
		fw.write("<LineStyle>" + '\n');
		fw.write("<color>ff00dbc5</color>" + '\n');
		fw.write("<width>0.0</width>" + '\n');
		fw.write("</LineStyle>" + '\n');
		fw.write("<PolyStyle>" + '\n');
		fw.write("<color>ff00dbc5</color>" + '\n');
		fw.write("</PolyStyle>" + '\n');
		fw.write("</Style>" + '\n');

		fw.write("<Style id=\"PolyStyle4\">" + '\n');
		fw.write("<LabelStyle>" + '\n');
		fw.write("<color>00000000</color>" + '\n');
		fw.write("<scale>0</scale>" + '\n');
		fw.write("</LabelStyle>" + '\n');
		fw.write("<LineStyle>" + '\n');
		fw.write("<color>ff009961</color>" + '\n');
		fw.write("<width>0.0</width>" + '\n');
		fw.write("</LineStyle>" + '\n');
		fw.write("<PolyStyle>" + '\n');
		fw.write("<color>ff009961</color>" + '\n');
		fw.write("</PolyStyle>" + '\n');
		fw.write("</Style>" + '\n');

		fw.write("<Style id=\"PolyStyle5\">" + '\n');
		fw.write("<LabelStyle>" + '\n');
		fw.write("<color>00000000</color>" + '\n');
		fw.write("<scale>0</scale>" + '\n');
		fw.write("</LabelStyle>" + '\n');
		fw.write("<LineStyle>" + '\n');
		fw.write("<color>ff006100</color>" + '\n');
		fw.write("<width>0.0</width>" + '\n');
		fw.write("</LineStyle>" + '\n');
		fw.write("<PolyStyle>" + '\n');
		fw.write("<color>ff006100</color>" + '\n');
		fw.write("</PolyStyle>" + '\n');
		fw.write("</Style>" + '\n');

	}

	public static void savePixelsToKML(Image image, String filepath) {
		savePixelsToKML(image.pixels, filepath, null);
	}

	public static void savePixelsToKML(List<Pixel> pixels, String filepath, Integer styleNumber) {
		try {
			FileWriter fw = null;
			fw = new FileWriter(filepath);
			fw.write(
					"<?xml version=" + comma + 1.0 + comma + " encoding=" + comma + "UTF-8" + comma + '?' + ">" + '\n');
			fw.write("<kml xmlns=" + comma + "http://earth.google.com/kml/2.0" + comma + ">" + '\n');
			fw.write("<Document>" + '\n');
			fw.write("<name>Polygons</name>" + '\n');
			fw.write("<open>1</open>" + '\n');

			addStyles(fw);

			for (Pixel px : pixels) {
				if (px != null && px.getCoordinate() != null) {
					fw.write("<Placemark>" + '\n');
					fw.write("<name>" + px.getValue() + "</name>" + '\n');
					fw.write("<Snippet maxLines =\"0\"> </Snippet>" + '\n');
					if (styleNumber == null) {
						fw.write("<styleUrl>" + "#PolyStyle" + px.getPolyStyle() + "</styleUrl>" + '\n');
					} else {
						fw.write("<styleUrl>" + "#PolyStyle" + styleNumber + "</styleUrl>" + '\n');
					}
					fw.write("<MultiGeometry>" + '\n');
					fw.write("<Polygon>" + '\n');
					fw.write("<tessellate> 1 </tessellate>" + '\n');
					fw.write("<outerBoundaryIs>" + '\n');
					fw.write("<LinearRing>" + '\n');

					fw.write("<coordinates>" + '\n');
					String coordinateSTR = "";

					for (Geo_Point geo : px.getOriginal()) {
						coordinateSTR += geo.getLon() + "," + geo.getLat() + ",0 ";
					}

					fw.write(coordinateSTR + '\n');
					fw.write("</coordinates>" + '\n');

					fw.write("</LinearRing>" + '\n');
					fw.write("</outerBoundaryIs>" + '\n');
					fw.write("</Polygon>" + '\n');
					fw.write("</MultiGeometry>" + '\n');
					fw.write("</Placemark>" + '\n');

				}
			}

			fw.write("</Document>" + '\n');
			fw.write("</kml>");

			fw.close();
		} catch (IOException e) {
			log.error(e);
		}
	}
	

}
