package ndviToolBox.runner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import geodesy.GeoConvertor;
import geodesy.Geo_Point;
import geodesy.Hemisphere;
import geodesy.UTM_Point;
import ndviToolBox.model.Image;
import ndviToolBox.model.ArrayUtils;
import ndviToolBox.model.KML_Statistics;
import ndviToolBox.model.NDVIServerResult;
import ndviToolBox.model.Pixel;
import ndviToolBox.model.PixelGrade;
import ndviToolBox.model.SuggestedLocation;
import ndviToolBox.model.SuggestedLocationStatus;

public class NonPivotLocationFinder extends LocationFinder {

	private static final Logger log = Logger.getLogger(NonPivotLocationFinder.class);
	private static final int defaultMaxDistanceFromLogger = 100000;

	private static int[][] quarterSTDQualityFilter;
	private static int[][] halfSTDQualityFilter;

	// locations matrices: 1 if pixel suits condition; 0 - otherwise
	private static int[][] locationsNearLogger;
	private static int[][] entirePlot;

	private static int[][] grade3x3;
	private static int[][] grade5x5;
	private static boolean DEBUG = false;

	@SuppressWarnings("unused")
	public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {

		String sourceKmlFile12264 = "C:\\Users\\geism\\Downloads\\Cubbie_AG_plot_12264.kml";
		UTM_Point loggerLocation12264 = new UTM_Point(659975.00000, 6911495.0000, 55, Hemisphere.South);

		String sourceKmlFile12269 = "C:\\Users\\geism\\Downloads\\Cubbie_AG_plot_12269.kml";
		UTM_Point loggerLocation12269 = new UTM_Point(661111.83932, 6906490.0000, 55, Hemisphere.South);

		String sourceKmlFile12265 = "C:\\Users\\geism\\Downloads\\Cubbie_AG_plot_12265.kml";
		UTM_Point loggerLocation12265 = new UTM_Point(660805.00000, 6910275.0000, 55, Hemisphere.South);

		String sourceKmlFile12270 = "C:\\Users\\geism\\Downloads\\Cubbie_AG_plot_12270.kml";
		UTM_Point loggerLocation12270 = new UTM_Point(662000.00001, 6907060.0001, 55, Hemisphere.South);

		String sourceKmlFile5105 = "C:\\Users\\geism\\Downloads\\Zikim_Plot_5105.kml";
		UTM_Point loggerLocation5105 = new UTM_Point(647134.78, 3498426.45, 36, Hemisphere.North);

		String kml = "C:\\Users\\misha\\Documents\\Debug2\\Plot_13326.kml";

		UTM_Point logger = GeoConvertor.getInstance().Deg2UTM(40.6841481663666, -98.1421516557207);

		File kmlFile = new File(sourceKmlFile5105);
		String content = "";

		if (kmlFile.exists() && kmlFile.canRead()) {
			Scanner scanner;
			try {
				scanner = new Scanner(kmlFile);
				content = scanner.useDelimiter("\\Z").next();
			} catch (FileNotFoundException e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}
		}

		// findLocation(kmlFile, loggerLocation12269, 12269);

		// SuggestedLocation sl = findLocation(content, loggerLocation5105, 350);
		SuggestedLocation sl = findLocation(new File(kml), logger, 350);

		long t1 = System.currentTimeMillis();

		NDVIServerResult sr = new NDVIServerResult();
		sr.setErrorMessage(sl.getStatus().name());
		sr.setSuggestedLocation(sl.getLocation());
		sr.setKmlContent(content);
		long t2 = System.currentTimeMillis();

		String json = sr.toJson();
		long t3 = System.currentTimeMillis();

		// log.debug(json);

		log.debug("put results to object " + (t2 - t1) + " msec");
		log.debug("convert object to json " + (t3 - t2) + " msec");

	}

	public static SuggestedLocation getErrorLocation() {
		SuggestedLocation res = new SuggestedLocation();
		res.setLocation(null);
		res.setStatus(SuggestedLocationStatus.ERROR_IN_PARSER);
		return res;
	}

	public static SuggestedLocation findLocation(String content, UTM_Point loggerPos, Integer maxDistanceFromLogger) {
		if (content == null) {
			return getErrorLocation();
		}

		long startTime = System.currentTimeMillis();
		log.info("Start...");

		Image image = new Image();
		try {
			image.parse(content);
		} catch (Exception e) {
			log.debug("Error during kml parser. No suggestion");
			return getErrorLocation();
		}

		SuggestedLocation res = getSuggestion(image, loggerPos, maxDistanceFromLogger).get(0);
		long finish = System.currentTimeMillis();
		log.info("Total - " + (finish - startTime) + " msec");
		return res;
	}

	public static SuggestedLocation findLocation(File sourceKML, UTM_Point loggerPos, Integer maxDistanceFromLogger) {
		if (sourceKML == null) {
			return getErrorLocation();
		}

		long startTime = System.currentTimeMillis();
		log.info("Start...");

		Image image = new Image();
		try {
			image.parse(sourceKML);
		} catch (Exception e) {
			log.debug("Error during kml parser. No suggestion");
			return getErrorLocation();
		}

		SuggestedLocation res = getSuggestion(image, loggerPos, maxDistanceFromLogger).get(0);
		long finish = System.currentTimeMillis();
		log.info("Total - " + (finish - startTime) + " msec");
		return res;
	}

	public static List<SuggestedLocation> getSuggestion(Image image, UTM_Point loggerPos,
			Integer maxDistanceFromLogger) {
		if (System.getenv("DEBUG") != null) {
			DEBUG = Boolean.parseBoolean(System.getenv("DEBUG"));
		}

		if (loggerPos != null && maxDistanceFromLogger == null) {
			maxDistanceFromLogger = defaultMaxDistanceFromLogger;
		}

		prepareData(loggerPos, maxDistanceFromLogger, image);

		List<SuggestedLocation> locations = new ArrayList<>();
		locations.add(twoStepsLocationFinder(loggerPos, image));

		return locations;

	}

	public static SuggestedLocation findLocationForManagementZones(Image image) {

		prepareData(null, defaultMaxDistanceFromLogger, image);

		return simpleLocationFinder(image);

	}

	private static void prepareData(UTM_Point loggerPos, Integer maxDistanceFromLogger, Image image) {

		SobelEdgeDetector sobel = new SobelEdgeDetector();
		sobel.process(image);

		if (DEBUG) {
			sobel.ImageWrite("C:\\Temp\\Direction.png", sobel.Direction);
			sobel.ImageWrite("C:\\Temp\\Magnitute.png", sobel.Magnitute);
		}
		image.cleanOutBorders(sobel, 0);

		int numX = image.numX();
		int numY = image.numY();

		// 1 if pixel suits conditions, 0 otherwise
		quarterSTDQualityFilter = new int[numX][numY];
		halfSTDQualityFilter = new int[numX][numY];
		entirePlot = new int[numX][numY];
		locationsNearLogger = new int[numX][numY];

		// each pixel is a mask result (number of ones in 3x3 or 5x5x
		// neighborhood)
		grade3x3 = new int[numX][numY];
		grade5x5 = new int[numX][numY];

		SuggestedLocation res = new SuggestedLocation();
		res.setLocation(null);
		res.setStatus(SuggestedLocationStatus.ERROR_VARIANCE_TOO_HIGH);

		image.calcStatistics();
		KML_Statistics statistics = image.getStatistics();

		log.info(statistics.toString());

		for (int i = 0; i < numX; i++) {
			for (int j = 0; j < numY; j++) {

				Pixel pix = image.getMatrix()[i][j];
				if (pix != null && pix.getValue() != null) {
					if (Math.abs(pix.getValue() - statistics.getMedian()) < 0.25 * statistics.getMAD()) {
						quarterSTDQualityFilter[i][j] = 1;
					}

					if ((pix.getValue() - statistics.getMedian()) < 0.5 * statistics.getMAD()) {
						halfSTDQualityFilter[i][j] = 1;
					}

					if (loggerPos != null) {
						double distanceFromLogger = Math
								.sqrt(Math.pow(pix.getCoordinate().getEasting() - loggerPos.getEasting(), 2)
										+ Math.pow(pix.getCoordinate().getNorthing() - loggerPos.getNorthing(), 2));
						if (distanceFromLogger < maxDistanceFromLogger) {
							locationsNearLogger[i][j] = 1;
						}
					}

					entirePlot[i][j] = 1;
				}
			}
		}

		if (DEBUG) {
			image.printMatrix(null);
		}
	}

	private static SuggestedLocation twoStepsLocationFinder(UTM_Point loggerPos, Image image) {
		UTM_Point suggestedLocationUTM;
		Geo_Point suggestedLocationGEO;

		SuggestedLocation res = new SuggestedLocation();
		List<PixelGrade> bestPixelsWithLogger = choseBestPixelByTwoMasks(locationsNearLogger);

		if (bestPixelsWithLogger != null && !bestPixelsWithLogger.isEmpty()) {

			suggestedLocationUTM = getBestLocation(image, bestPixelsWithLogger);

			if (suggestedLocationUTM != null) {
				System.out.println("Suggested locaton is " + suggestedLocationUTM.getEasting() + " "
						+ suggestedLocationUTM.getNorthing() + " score " + bestPixelsWithLogger.get(0).getGrade());
				suggestedLocationGEO = GeoConvertor.getInstance().UTM2Deg(suggestedLocationUTM);
				System.out.println("Lat: " + suggestedLocationGEO.getLat() + " Lon: " + suggestedLocationGEO.getLon());
				res.setLocation(suggestedLocationGEO);
				res.setStatus(SuggestedLocationStatus.OK);

				List<PixelGrade> bestPixelsWithoutLogger = choseBestPixelByTwoMasks(entirePlot);
				if (!bestPixelsWithoutLogger.isEmpty() && bestPixelsWithoutLogger.get(0).getGrade() > 0) {
					if (bestPixelsWithLogger.get(0).getGrade() / bestPixelsWithoutLogger.get(0).getGrade() < 0.7) {
						log.debug("Fix logger location!");
						res.setStatus(SuggestedLocationStatus.WARN_FIX_LOGGER_LOCATION);
					}
				}

			} else {
				log.debug("No suggestion");
			}
		} else {
			log.debug("No suggestion");
			res.setStatus(SuggestedLocationStatus.ERROR_VARIANCE_TOO_HIGH);
		}
		return res;
	}

	private static UTM_Point getBestLocation(Image image, List<PixelGrade> listOfPixels) {

		Double max_ndvi = null;
		int max_ndvi_i = listOfPixels.get(0).getI();
		int max_ndvi_j = listOfPixels.get(0).getJ();

		for (PixelGrade pxGrade : listOfPixels) {
			Pixel px = image.getMatrix()[pxGrade.getI()][pxGrade.getJ()];
			if (px != null) {
				if (max_ndvi == null || px.getValue() > max_ndvi) {
					max_ndvi = px.getValue();
					max_ndvi_i = pxGrade.getI();
					max_ndvi_j = pxGrade.getJ();
				}
			}
		}

		return getLocationByIJ(image, max_ndvi_i, max_ndvi_j);

	}

	private static UTM_Point getLocationByIJ(Image image, int i, int j) {
		Pixel px = image.getMatrix()[i][j];
		if (px != null) {
			return px.getCoordinate();
		} else {

			return new UTM_Point(image.getMin().getEasting() + i * image.getResolution(),
					image.getMin().getNorthing() + j * image.getResolution(), image.getMin().getZone(),
					image.getMin().getHemisphere());
		}
	}

	private static SuggestedLocation simpleLocationFinder(Image image) {
		UTM_Point suggestedLocationUTM;
		Geo_Point suggestedLocationGEO;
		SuggestedLocation res = new SuggestedLocation();
		List<PixelGrade> bestPixels = chooseBestPixelByOneMask(entirePlot);

		if (!bestPixels.isEmpty() && bestPixels.get(0).getGrade() > 0) {

			suggestedLocationUTM = getBestLocation(image, bestPixels);

			if (suggestedLocationUTM != null) {
				System.out.println("Suggested locaton is " + suggestedLocationUTM.getEasting() + " "
						+ suggestedLocationUTM.getNorthing() + " score " + bestPixels.get(0).getGrade());
				suggestedLocationGEO = GeoConvertor.getInstance().UTM2Deg(suggestedLocationUTM);
				System.out.println("Lat: " + suggestedLocationGEO.getLat() + " Lon: " + suggestedLocationGEO.getLon());
				res.setLocation(suggestedLocationGEO);
				res.setStatus(SuggestedLocationStatus.OK);

			} else {
				log.debug("No suggestion");
			}
		} else {
			log.debug("No suggestion");
		}
		return res;
	}

	private static List<PixelGrade> choseBestPixelByTwoMasks(int[][] pixelsMatrix) {
		List<PixelGrade> maxGradeAmongImage = null;
		List<PixelGrade> l5_quarterMaxGrade;
		List<PixelGrade> l3_halfMaxGrade;
		List<PixelGrade> l5_halfMaxGrade;
		List<PixelGrade> l3_quarterMaxGrade = calcMaxGrade(ArrayUtils.mult(quarterSTDQualityFilter, pixelsMatrix), 3);
		if (l3_quarterMaxGrade.isEmpty()) {
			log.debug("Max grade of 3x3 quarterSTD is empty");
		} else {
			log.debug("Max grade of 3x3 quarterSTD " + l3_quarterMaxGrade.get(0).getGrade());
		}

		if (l3_quarterMaxGrade.isEmpty() || l3_quarterMaxGrade.get(0).getGrade() < 3) {
			l3_halfMaxGrade = calcMaxGrade(ArrayUtils.mult(halfSTDQualityFilter, pixelsMatrix), 3);

			if (l3_halfMaxGrade.isEmpty()) {
				log.debug("Max grade of 3x3 l3_halfMaxScore is empty");
			} else {
				log.debug("Max grade of 3x3 l3_halfMaxScore " + l3_halfMaxGrade.get(0).getGrade());
			}

			if (l3_halfMaxGrade.isEmpty() || l3_halfMaxGrade.get(0).getGrade() < 3) {
				log.info("Variance is too high");
				return null;

			} else {
				l5_halfMaxGrade = calcMaxGrade(ArrayUtils.mult(halfSTDQualityFilter, pixelsMatrix), 5);
				log.debug("Max grade of 5x5 halfSTD " + l5_halfMaxGrade.get(0).getGrade());

			}
		} else {
			if (l3_quarterMaxGrade.size() == 1 && l3_quarterMaxGrade.get(0).getGrade() > 7) {
				maxGradeAmongImage = l3_quarterMaxGrade;
			} else {
				l5_quarterMaxGrade = calcMaxGrade(ArrayUtils.mult(quarterSTDQualityFilter, pixelsMatrix), 5);
				log.debug("Max grade of 5x5 quarterSTD " + l5_quarterMaxGrade.get(0).getGrade());

			}
		}

		if (maxGradeAmongImage == null) {
			maxGradeAmongImage = sumAndFindMax();
		}

		log.debug(maxGradeAmongImage.size() + " pixels with grade " + maxGradeAmongImage.get(0).getGrade());

		return maxGradeAmongImage;
	}

	private static List<PixelGrade> chooseBestPixelByOneMask(int[][] pixelsMatrix) {

		List<PixelGrade> maxGradeAmongImage = null;
		List<PixelGrade> l3_quarterMaxGrade = calcMaxGrade(ArrayUtils.mult(quarterSTDQualityFilter, pixelsMatrix), 3);
		if (l3_quarterMaxGrade.isEmpty()) {
			log.debug("Max grade of 3x3 quarterSTD is empty");
		} else {
			log.debug("Max grade of 3x3 quarterSTD " + l3_quarterMaxGrade.get(0).getGrade());
		}

		if (l3_quarterMaxGrade.isEmpty()) {
			maxGradeAmongImage = null;
		} else {
			maxGradeAmongImage = l3_quarterMaxGrade;
		}

		if (maxGradeAmongImage != null) {
			log.debug(maxGradeAmongImage.size() + " pixels with grade " + maxGradeAmongImage.get(0).getGrade());
			return maxGradeAmongImage;
		} else {
			return new ArrayList<PixelGrade>();
		}

	}

	private static List<PixelGrade> sumAndFindMax() {
		List<PixelGrade> maxScoreList = new ArrayList<PixelGrade>();
		maxScoreList.add(new PixelGrade(0, 0, 0));
		for (int i = 0; i < grade5x5.length; i++) {
			for (int j = 0; j < grade5x5[i].length; j++) {
				if (grade3x3[i][j] + grade5x5[i][j] > maxScoreList.get(0).getGrade()) {
					maxScoreList.clear();
					maxScoreList.add(new PixelGrade(i, j, grade3x3[i][j] + grade5x5[i][j]));
				} else if (grade3x3[i][j] + grade5x5[i][j] == maxScoreList.get(0).getGrade()) {
					maxScoreList.add(new PixelGrade(i, j, grade3x3[i][j] + grade5x5[i][j]));
				}
			}
		}

		if (maxScoreList.get(0).getGrade() > 0) {
			return maxScoreList;
		}
		return new ArrayList<PixelGrade>();
	}

	private static int calcMask(int[][] matrix, int maskSize, int row, int col) {
		int score = 0;
		int halfMaskSize = (int) maskSize / 2;
		for (int i = row - halfMaskSize; i <= row + halfMaskSize; i++) {
			for (int j = col - halfMaskSize; j <= col + halfMaskSize; j++) {
				score += matrix[i][j];
			}

		}

		return score;
	}

	private static List<PixelGrade> calcMaxGrade(int[][] matrix, int maskSize) {
		int maxScore = 0;
		int halfMaskSize = (int) maskSize / 2;

		List<PixelGrade> maxScorePixels = new ArrayList<PixelGrade>();

		for (int i = halfMaskSize; i < matrix.length - halfMaskSize; i++) {
			for (int j = halfMaskSize; j < matrix[i].length - halfMaskSize; j++) {
				int score = calcMask(matrix, maskSize, i, j);
				if (maskSize == 3) {
					grade3x3[i][j] = score;
				} else {
					grade5x5[i][j] = score;
				}

				if (score > maxScore) {
					maxScore = score;
					maxScorePixels.clear();
					maxScorePixels.add(new PixelGrade(i, j, score));
				} else if (score == maxScore && score != 0) {
					maxScorePixels.add(new PixelGrade(i, j, score));
				}
			}
		}

		return maxScorePixels;

	}

}
