package ndviToolBox.model;

public enum SuggestedLocationStatus {
	
	OK,ERROR_NO_LOGGER,ERROR_IN_PARSER,WARN_FIX_LOGGER_LOCATION,ERROR_VARIANCE_TOO_HIGH

}
