package ndviToolBox.model;

public class ArrayUtils {
	public static int[][] mult(int[][] a, int[][] b) {
		int[][] res = new int[a.length][a[0].length];
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				res[i][j] = a[i][j] * b[i][j];
			}
		}

		return res;
	}

	
	public static void printMatrix(String label,int[][] clusteredMatrix) {
		System.out.println(label);

		for (int r = 0; r < clusteredMatrix.length; r++) {
			String row = "";
			for (int c = 0; c < clusteredMatrix[0].length; c++) {
				if (clusteredMatrix[r][c] == 0) {
					row += "  ";
				} else {
					row += clusteredMatrix[r][c] + " ";
				}
			}
			System.out.println(row);
		}
	}

	public static void printMatrix(String label,double[][] clusteredMatrix) {
		System.out.println(label);
		for (int r = 0; r < clusteredMatrix.length; r++) {
			String row = "";
			for (int c = 0; c < clusteredMatrix[0].length; c++) {
				if (clusteredMatrix[r][c] == 0) {
					row += " 0 ";
				} else {
					row += " 1 ";
				}
			}
			System.out.println(row);
		}
	}

	public static void printMatrix1(String label,Pixel[][] pixels) {
		System.out.println(label);

		for (int r = 0; r < pixels.length; r++) {
			String row = "";
			for (int c = 0; c < pixels[0].length; c++) {
				if (pixels[r][c] == null) {
					row += "XXX";
				} else {
					row += pixels[r][c].getValue() + " ";
				}
			}
			System.out.println(row);
		}
	}

	public static void printMatrix(String label,Pixel[][] pixels) {
		System.out.println(label);

		for (int r = 0; r < pixels.length; r++) {
			String row = "";
			for (int c = 0; c < pixels[0].length; c++) {
				if (pixels[r][c] == null) {
					row += "X";
				} else {
					row += "-";
				}
			}
			System.out.println(row);
		}
	}
}
