package ndviToolBox.model;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.jsoup.Jsoup;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import geodesy.GeoConvertor;
import geodesy.Geo_Point;
import geodesy.Hemisphere;
import geodesy.UTM_Point;
import ndviToolBox.runner.SobelEdgeDetector;

public class Image {
	private static final double NORMALIZATION_CONSTANT = 0.67448975019608171;
	public List<Pixel> pixels;
	public double[] values;
	public Pixel[][] matrix = null;
	private float[][] distances = null;
	private KML_Statistics statistics = null;
	protected UTM_Point min;
	protected UTM_Point max;
	public int maxIndex = 0;
	public Set<Integer> pixelsHashSet = new HashSet<Integer>();

	protected static final String NEW_LINE_SEPARATOR = "\n";
	private final int pixelResolution = 10;
	private final double max_ndvi_value_for_foot_path = 0.5;
	private int duplcatedPixelsCounter = 0;
	protected ImageType type = ImageType.NDVI;
	protected String imageName;

	public Image() {
		pixels = new ArrayList<Pixel>();
		values = null;

		// Initialize MBR
		min = new UTM_Point(Double.MAX_VALUE, Double.MAX_VALUE, 0, Hemisphere.North);
		max = new UTM_Point(Double.MIN_VALUE, Double.MIN_VALUE, 0, Hemisphere.North);

		statistics = new KML_Statistics();
	}
	// Lat/Long :
	// kml->Document->Placemark->MultiGeometry->Polygon->outerBoundaryIs->LinearRing->coordinates
	// NDVI value :
	// kml->Document->Placemark->name

	public void parse(File file) throws ParserConfigurationException, SAXException, IOException {

		// Get Document Builder
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

		// Build Document
		Document document = dBuilder.parse(file.getAbsoluteFile().getAbsolutePath());

		parseDocument(document);
		convertToMatrix();

		imageName= file.getName().substring(0, file.getName().lastIndexOf("."));

	}

	public void parse(String kmlContent) throws ParserConfigurationException, SAXException, IOException {

		// Get Document Builder
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(kmlContent));
		// Build Document
		Document document = dBuilder.parse(is);

		parseDocument(document);

		convertToMatrix();
	}

	private void convertToMatrix() {
		// transform pixels list to 2x2 array

		// get 4 MBR corners
		double minEasting = getMin().getEasting();
		double maxEasting = getMax().getEasting();

		double maxNorthing = getMax().getNorthing();
		double minNorthing = getMin().getNorthing();

		// round to near up/down pixelResolution
		minEasting = minEasting - minEasting % pixelResolution;
		maxEasting = maxEasting - maxEasting % pixelResolution + pixelResolution;
		minNorthing = minNorthing - minNorthing % pixelResolution;
		maxNorthing = maxNorthing - maxNorthing % pixelResolution + pixelResolution;

		// calculate array dimensions
		double width = maxEasting - minEasting;
		double height = maxNorthing - minNorthing;

		int numY = (int) width / pixelResolution;
		int numX = (int) height / pixelResolution;

		// raw NDVI values
		matrix = new Pixel[numX + 4][numY + 4];

		for (Pixel pix : pixels) {
			double east = pix.getCoordinate().getEasting();
			double north = pix.getCoordinate().getNorthing();

			long deltaY = Math.round(east - minEasting);
			long deltaX = Math.round(maxNorthing - north);

			int Y = (int) (deltaY / pixelResolution);
			int X = (int) (deltaX / pixelResolution);

			if (X >= 4 && X < matrix.length - 4 && Y >= 4 && Y < matrix[0].length - 4) {
				matrix[X + 2][Y + 2] = pix;
			}

		}

	}

	protected void parseDocument(Document document) {
		// Normalize the XML Structure; It's just too important !!
		document.getDocumentElement().normalize();

		int pointId = 0;

		// Get all Placemark nodes
		NodeList nList = document.getElementsByTagName("Placemark");

		// run through placemarks and parse each one to utm point
		for (int temp = 0; temp < nList.getLength(); temp++) {

			Node nNode = nList.item(temp);

			if (nNode.getNodeType() == Node.ELEMENT_NODE && nNode.getNodeName() == "Placemark") {

				Node multiGeometry = getInnerNode(nNode.getChildNodes(), "MultiGeometry");
				Double val = null;
				Node description = getInnerNode(nNode.getChildNodes(), "description");
				if (description == null) {
					Node value = getInnerNode(nNode.getChildNodes(), "name");
					if (value != null) {
						val = Double.parseDouble(value.getTextContent());
					}
				} else {
					val = parsePlacemarkDescription(description.getTextContent());
				}

				Node style = getInnerNode(nNode.getChildNodes(), "styleUrl");
				int nStyle = parseStyle(style.getTextContent());

				Node polygon = getInnerNode(multiGeometry.getChildNodes(), "Polygon");
				Node outerBoundaryIs = getInnerNode(polygon.getChildNodes(), "outerBoundaryIs");
				Node linearRing = getInnerNode(outerBoundaryIs.getChildNodes(), "LinearRing");
				Node coordinates = getInnerNode(linearRing.getChildNodes(), "coordinates");

				UTM_Point utmPoint = null;
				List<Geo_Point> originalCoordinates = null;
				if (coordinates != null) {
					String coordinateStr = coordinates.getTextContent();
					utmPoint = parseCoordinatesString(coordinateStr);
					originalCoordinates = getOriginCoordinates(coordinateStr);
					double easting = utmPoint.getEasting();
					double northing = utmPoint.getNorthing();

					if (easting < min.getEasting()) {
						min.setEasting(easting);
					}

					if (easting > max.getEasting()) {
						max.setEasting(easting);
					}

					if (northing < min.getNorthing()) {
						min.setNorthing(northing);
					}

					if (northing > max.getNorthing()) {
						max.setNorthing(northing);
					}

				}

				Pixel pixel = new Pixel();
				pixel.setCoordinate(utmPoint);
				pixel.setValue(val);
				pixel.setId(pointId);
				pixel.setPolyStyle(nStyle);
				pixel.setOriginal(originalCoordinates);
				if (pointId > maxIndex) {
					maxIndex = pointId;
				}
				int hashcode = pixel.hashCode();

				boolean isDuplicated = pixelsHashSet.contains(hashcode);
				pixelsHashSet.add(hashcode);

				if (isDuplicated) {
					duplcatedPixelsCounter++;
				} else {
					pixels.add(pixel);
					pointId++;

				}

			}

			values = new double[pixels.size()];
			int index = 0;
			for (Pixel px : pixels) {
				values[index++] = px.getValue();
			}
			if (!pixels.isEmpty()) {
				min.setZone(pixels.get(0).getCoordinate().getZone());
				min.setHemisphere(pixels.get(0).getCoordinate().getHemisphere());
				max.setZone(pixels.get(0).getCoordinate().getZone());
				max.setHemisphere(pixels.get(0).getCoordinate().getHemisphere());

			}

		}

		System.out.println("Found and removed " + duplcatedPixelsCounter + " duplicated pixels");

	}

	private int parseStyle(String styleStr) {
		// <styleUrl>#PolyStyle5</styleUrl>

		if (styleStr == null || !styleStr.contains("#PolyStyle")) {
			return 0;
		}

		String style = styleStr.replace("#PolyStyle", "");

		return Integer.parseInt(style);
	}

	private static Double parsePlacemarkDescription(String textContent) {
		org.jsoup.nodes.Document doc = Jsoup.parse(textContent);
		for (org.jsoup.nodes.Element e : doc.getAllElements()) {
			for (int i = 0; i < e.childNodes().size(); i++) {
				org.jsoup.nodes.Node n = e.childNode(i);
				if (n.toString().equals("<td>RASTERVALU</td>")) {

					String sVal = e.childNode(i + 2).toString();
					sVal = sVal.replace("<td>", "");
					sVal = sVal.replace("</td>", "");

					Double val = Double.parseDouble(sVal);
					return val;

				}
			}

		}

		return null;
	}

	protected static UTM_Point parseCoordinatesString(String coordinateStr) {
		String pattern = "[\\s]+";

		String[] points = coordinateStr.split(pattern);
		double lat = 0.0;
		double lon = 0.0;

		int pointsCounter = 0;

		for (int i = 0; i < points.length - 1; i++) {
			if (points[i].isEmpty()) {
				continue;
			}
			Geo_Point geoPoint = parsePoint(points[i]);
			if (geoPoint != null) {
				lat += geoPoint.getLat();
				lon += geoPoint.getLon();
				pointsCounter++;
			}
		}

		Geo_Point averageGeo = new Geo_Point(lat / (pointsCounter),lon / (pointsCounter));

		UTM_Point utmPoint = GeoConvertor.getInstance().Deg2UTM(averageGeo.getLat(), averageGeo.getLon());

		return utmPoint;

	}

	protected static List<Geo_Point> getOriginCoordinates(String coordinateStr) {
		String pattern = "[\\s]+";

		String[] points = coordinateStr.split(pattern);

		List<Geo_Point> originPoints = new ArrayList<Geo_Point>();

		for (int i = 0; i < points.length - 1; i++) {
			if (points[i].isEmpty()) {
				continue;
			}
			Geo_Point geoPoint = parsePoint(points[i]);
			if (geoPoint != null) {
				originPoints.add(geoPoint);
			}
		}

		return originPoints;

	}

	protected static Node getInnerNode(NodeList nodes, String innerNodeName) {
		Node res = null;

		if (nodes == null) {
			return res;
		}
		for (int index = 0; index < nodes.getLength(); index++) {
			Node nNode = nodes.item(index);

			if (nNode.getNodeType() == Node.ELEMENT_NODE && nNode.getNodeName() == innerNodeName) {
				return nNode;
			}
		}

		return res;
	}

	protected static Geo_Point parsePoint(String point) {
		String[] coords = point.split(",");
		if (coords.length < 2) {
			return null;
		} else {
			Geo_Point geo = new Geo_Point(0.0,0.0);
			try {
				geo.setLon(Double.parseDouble(coords[0]));
				geo.setLat(Double.parseDouble(coords[1]));
				return geo;
			} catch (NumberFormatException nfe) {
				return null;
			}
		}
	}

	public List<Pixel> getPixels() {
		return pixels;
	}

	public double[] getValues() {
		return values;
	}

	public UTM_Point getMin() {
		return min;
	}

	public UTM_Point getMax() {
		return max;
	}

	public KML_Statistics parseFromDebugFile(String csvFile) throws IOException {

		String line = "";
		String cvsSplitBy = ",";
		KML_Statistics plot_statistics = new KML_Statistics();

		BufferedReader br = new BufferedReader(new FileReader(csvFile));
		while ((line = br.readLine()) != null) {
			String[] dataParams = line.split(cvsSplitBy);

			try {
				for (int i = 0; i < dataParams.length; i++) {
					Double val = Double.parseDouble(dataParams[i]);
					if (val > 0) {
						plot_statistics.addValue(val);
					}

				}
			} catch (Exception ex) {
				continue; // nothing to do
			}
		}

		// calculate MAD as median of |x-med(x)|
		DescriptiveStatistics statForMAD = new DescriptiveStatistics();
		for (double val : plot_statistics.descriptiveStatistics.getValues()) {
			statForMAD.addValue(Math.abs(val - plot_statistics.getMedian()));
		}

		plot_statistics.setMAD(statForMAD.getPercentile(50) / NORMALIZATION_CONSTANT);

		br.close();

		return plot_statistics;
	}

	public void cleanOutBorders(SobelEdgeDetector sobel) {
		cleanOutBorders(sobel, 1);
	}

	public void cleanOutBorders(SobelEdgeDetector sobel, int radius) {
		if (sobel == null) {
			cleanOutBorders();
			return;
		}

		pixels.clear();

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (isEdge(i, j, sobel, radius)) {
					matrix[i][j] = null;
				} else {
					if (matrix[i][j] != null) {
						pixels.add(matrix[i][j]);
					}
				}
			}
		}

		values = new double[pixels.size()];
		int index = 0;
		for (Pixel px : pixels) {
			values[index++] = px.getValue();
		}

		return;
	}

	private boolean isEdge(int r, int c, SobelEdgeDetector sobel, int radius) {
		for (int i = Math.max(0, r - radius); i <= Math.min(sobel.Magnitute[0].length - 1, r + radius); i++) {
			for (int j = Math.max(0, c - radius); j <= Math.min(sobel.Magnitute.length - 1, c + radius); j++) {
				if (sobel.Magnitute[j][i] > 0) {
					return true;
				}
			}
		}

		return false;
	}

	public List<UTM_Point> getBorders(SobelEdgeDetector sobel) {
		List<UTM_Point> border = new ArrayList<UTM_Point>();

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (sobel.Magnitute[j][i] > 0) {
					border.add(matrix[i][j].getCoordinate());
				}
			}
		}

		return border;
	}

	public void cleanOutBorders() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] == null) {
					continue;
				} else {
					matrix[i][j] = null;

					if (j < matrix[i].length - 1) {
						matrix[i][j + 1] = null;
					}
					break;
				}
			}

			for (int j = matrix[i].length - 1; j > 1; j--) {
				if (matrix[i][j] == null) {
					continue;
				} else {

					matrix[i][j] = null;
					if (j > 1) {
						matrix[i][j - 1] = null;
					}
					break;
				}
			}

		}

		for (int j = 0; j < matrix[0].length; j++) {
			for (int i = 0; i < matrix.length; i++) {

				if (matrix[i][j] == null) {
					continue;
				} else {
					matrix[i][j] = null;

					if (i < matrix[i].length - 1) {
						matrix[i + 1][j] = null;
					}
					break;
				}
			}

			for (int i = matrix.length - 1; i > 1; i--) {
				if (matrix[i][j] == null) {
					continue;
				} else {

					matrix[i][j] = null;
					if (i > 1) {
						matrix[i - 1][j] = null;
					}
					break;
				}
			}
		}

		return;

	}

	public void printMatrix(String filePath) {
		if (filePath == null) {
			filePath = "C:\\Temp\\";
		}

		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(filePath + imageName+ "_pixels.txt");

			for (int i = 0; i < matrix.length; i++) {
				String row = "";
				for (int j = 0; j < matrix[i].length; j++) {
					if (matrix[i][j] == null) {
						row += "-\t";
					} else {
						row += "X\t";
					}

				}
				fileWriter.append(row);
				fileWriter.append(NEW_LINE_SEPARATOR);
			}

		} catch (Exception e) {
			System.out.println("Error in FileWriter");
			e.printStackTrace();
		} finally {

			try {
				if (fileWriter != null) {
					fileWriter.flush();
					fileWriter.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		try {
			fileWriter = new FileWriter(filePath + imageName+ "_values.txt");

			for (int i = 0; i < matrix.length; i++) {
				String row = "";
				for (int j = 0; j < matrix[i].length; j++) {
					if (matrix[i][j] == null) {
						row += "-\t";
					} else {
						row += matrix[i][j].getValue() + "\t";

					}

				}
				fileWriter.append(row);
				fileWriter.append(NEW_LINE_SEPARATOR);
			}

		} catch (Exception e) {
			System.out.println("Error in FileWriter");
			e.printStackTrace();
		} finally {

			try {
				if (fileWriter != null) {
					fileWriter.flush();
					fileWriter.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		try {
			fileWriter = new FileWriter(filePath + imageName+ "_coordinates.txt");

			for (int i = 0; i < matrix.length; i++) {
				String row = "";
				for (int j = 0; j < matrix[i].length; j++) {
					if (matrix[i][j] == null) {
						row += "-\t";
					} else {
						row += "[" + matrix[i][j].getCoordinate().getEasting() + ","
								+ matrix[i][j].getCoordinate().getNorthing() + "]\t";
					}

				}
				fileWriter.append(row);
				fileWriter.append(NEW_LINE_SEPARATOR);
			}

		} catch (Exception e) {
			System.out.println("Error in FileWriter");
			e.printStackTrace();
		} finally {

			try {
				if (fileWriter != null) {
					fileWriter.flush();
					fileWriter.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	/**
	 * This is a public method, so an application may decide when to call it. For
	 * example, before or after border cleaning
	 * 
	 * @return
	 */
	public void calcStatistics() {

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] != null) {
					statistics.addValue(matrix[i][j].getValue());
				}
			}
		}

		// calculate MAD as median of |x-med(x)|
		DescriptiveStatistics statForMAD = new DescriptiveStatistics();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] != null) {
					statForMAD.addValue(Math.abs(matrix[i][j].getValue() - statistics.getMedian()));
				}
			}
		}

		statistics.setMAD(statForMAD.getPercentile(50) / NORMALIZATION_CONSTANT);

	}

	public KML_Statistics getStatistics() {
		return statistics;

	}

	public Pixel[][] getMatrix() {
		return matrix;
	}

	public int numX() {
		if (matrix != null) {
			return matrix.length;
		}
		return 0;
	}

	public int numY() {
		if (matrix[0] != null) {
			return matrix[0].length;
		}
		return 0;
	}

	public void calcPairwiseDistances() {
		distances = new float[maxIndex + 1][maxIndex + 1];
		for (int i = 0; i < pixels.size(); i++) {
			Pixel a = pixels.get(i);

			for (int j = 0; j < pixels.size(); j++) {
				Pixel b = pixels.get(j);
				distances[a.getId()][b.getId()] = (float) Math.abs(a.getValue() - b.getValue());
			}
		}

	}

	public float[][] getDistances() {
		return distances;
	}

	public void limitBoundary(UTM_Point min, UTM_Point max) {
		this.min = min;
		this.max = max;

		convertToMatrix();
	}

	public BufferedImage toBinaryBufferedImage() {

		BufferedImage res = new BufferedImage(matrix.length, matrix[0].length, BufferedImage.TYPE_INT_RGB);

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				if (matrix[i][j] != null) {
					res.setRGB(i, j, Color.BLACK.getRGB());
				} else {
					res.setRGB(i, j, Color.WHITE.getRGB());
				}

			}
		}

		return res;

	}

	public int getResolution() {
		return pixelResolution;
	}

	public void cleanFootPathPixels(RGB_Image rgbImage) {
		pixels.clear();

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (rgbImage.isPixelOnPath(i, j) && matrix[i][j] != null
						&& matrix[i][j].getValue() < max_ndvi_value_for_foot_path) {
					matrix[i][j] = null;
				} else {
					if (matrix[i][j] != null) {
						pixels.add(matrix[i][j]);
					}

				}
			}
		}

		values = new double[pixels.size()];
		int index = 0;
		for (Pixel px : pixels) {
			values[index++] = px.getValue();
		}

	}

}
