package ndviToolBox.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.ObjectMapper;

import geodesy.Geo_Point;

public class GroupOfPixel {
	private int groupId;
	private int size;
	private List<Geo_Point> pixels;

	public int getGroup_id() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public GroupOfPixel() {
		pixels = new ArrayList<Geo_Point>();
	}

	public void addPoint(Geo_Point point) {
		pixels.add(point);
	}

	public List<Geo_Point> getPixels() {
		return pixels;
	}
	
	@JsonIgnore
	public String toJson() {
		String json = null;

		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return json;

	}
}
