package ndviToolBox.runner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.apache.log4j.Logger;
import com.phytech.model.ProjectMetadata;
import com.phytech.model.Enumerations.PlotAgeEstimation;
import com.phytech.model.Enumerations.CropType;
import com.phytech.utils.Constants;
import com.phytech.utils.PhytechApi;
import com.phytech.utils.PropertiesManager;

import ndviToolBox.model.Image;

public class PlotAgeEstimator {
	private static final Logger log = Logger.getLogger(PlotAgeEstimator.class);
	private static boolean inited = false;
	private static double matureNDVIAlmond = Constants.MATURE_NDVI_ALMOND;
	private static double matureNDVIWalnut = Constants.MATURE_NDVI_WALNUT;
	private static double matureNDVIAvocado = Constants.MATURE_NDVI_AVOCADO;

	public static void main(String[] args) {
		PropertiesManager.initConfiguration(System.getenv(Constants.CONFIGURATION_FILE_VARIABLE_NAME));

		String kml = "C:\\Users\\misha\\Documents\\Debug\\37276-2018-09-13.kml";

		ProjectMetadata pmd = PhytechApi.getProjectMetaData(37276);

		PlotAgeEstimation ageEstimation = estimatePlotAge(kml, pmd.getCropType());

		log.info(ageEstimation.name());

		System.out.println("Done");
	}

	private static void initParams() {

		if (System.getenv(Constants.MATURE_NDVI_FOR_ALMOND_ENV_VARIABLE_NAME) != null) {
			matureNDVIAlmond = Double.parseDouble(System.getenv(Constants.MATURE_NDVI_FOR_ALMOND_ENV_VARIABLE_NAME));
		}

		if (System.getenv(Constants.MATURE_NDVI_FOR_WALNUT_ENV_VARIABLE_NAME) != null) {
			matureNDVIWalnut = Double.parseDouble(System.getenv(Constants.MATURE_NDVI_FOR_WALNUT_ENV_VARIABLE_NAME));
		}

		if (System.getenv(Constants.MATURE_NDVI_FOR_AVOCADO_ENV_VARIABLE_NAME) != null) {
			matureNDVIAvocado = Double.parseDouble(System.getenv(Constants.MATURE_NDVI_FOR_AVOCADO_ENV_VARIABLE_NAME));
		}

		log.info("Mature NDVI average for Almond starts " + matureNDVIAlmond);
		log.info("Mature NDVI average for Walnut starts " + matureNDVIWalnut);
		log.info("Mature NDVI average for Avocado starts " + matureNDVIAvocado);

	}

	public static PlotAgeEstimation getCropAge(File ndviFile, CropType crop) {

		String content = "";

		if (ndviFile.exists() && ndviFile.canRead()) {
			Scanner scanner;
			try {
				scanner = new Scanner(ndviFile);
				content = scanner.useDelimiter("\\Z").next();
			} catch (FileNotFoundException e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}
		}

		return estimatePlotAge(content, crop);

	}

	public static PlotAgeEstimation estimatePlotAge(String ndviContent, CropType crop) {
		if (!inited) {
			initParams();
			inited = true;
		}

		Image image = new Image();

		// parse 2 NDVI files
		try {
			image.parse(ndviContent);

		} catch (Exception e) {
			log.error("Error during kml parser");
			return PlotAgeEstimation.UNKNOWN;

		}

		if (image.getPixels().isEmpty()) {
			log.error("Empty file");
			return PlotAgeEstimation.UNKNOWN;
		}

		// clean external borders
		SobelEdgeDetector extSobel = new SobelEdgeDetector();
		extSobel.process(image);

		// remove pixels near the edges (external borders of NDVI)
		image.cleanOutBorders(extSobel);

		// calculate statistics for NDVI file
		image.calcStatistics();
		double ndviAverage = image.getStatistics().getAverage();

		switch (crop) {
		case ALMOND:
			if (ndviAverage >= matureNDVIAlmond) {
				return PlotAgeEstimation.MATURE;
			} else {
				return PlotAgeEstimation.YOUNG;
			}
		case WALNUT:
			if (ndviAverage >= matureNDVIWalnut) {
				return PlotAgeEstimation.MATURE;
			} else {
				return PlotAgeEstimation.YOUNG;
			}
		case AVOCADO:
			if (ndviAverage >= matureNDVIAvocado) {
				return PlotAgeEstimation.MATURE;
			} else {
				return PlotAgeEstimation.YOUNG;
			}
		default:
			return PlotAgeEstimation.UNKNOWN;

		}

	}

}
